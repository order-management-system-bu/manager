package uk.ac.bournemouth.i7626893.oms.manager.json.menu;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuCategory;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;

/**
 * Class is responsible for providing the JSON conversion
 * logic for all instances of MenuCategory.
 */
public class MenuCategoryJson implements JsonReader<MenuCategory>, JsonWriter<MenuCategory> {
    @Override
    public Optional<MenuCategory> read(JSONObject object) {
        return Optional.of(new MenuCategory(
                JsonUtils.stringOrNull(object, "id"),
                JsonUtils.stringOrNull(object, "title"),
                JsonUtils.stringOrNull(object, "description")
        ));
    }

    @Override
    public Optional<JSONObject> write(MenuCategory model) {
        try {
            JSONObject json = new JSONObject();
            JsonUtils.putIfNotNull(json, "title", model.getTitle());
            JsonUtils.putIfNotNull(json, "description", model.getDescription());
            return Optional.of(json);
        } catch (JSONException e) {
            return Optional.empty();
        }
    }
}
