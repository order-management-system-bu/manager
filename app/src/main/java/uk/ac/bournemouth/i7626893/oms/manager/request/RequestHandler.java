package uk.ac.bournemouth.i7626893.oms.manager.request;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.json.validation.ValidationFaultJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.validation.ValidationFault;
import uk.ac.bournemouth.i7626893.oms.manager.model.validation.ValidationGroup;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.DialogUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.RequestBuilder;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for creating an pushing
 * M related requests to the request queue.
 */
public abstract class RequestHandler<M extends Model> {

    public static final String FAILURE = "An unexpected network fault has occurred.";
    public static final String UNAUTHORIZED = "You are not authorized to access this.";
    protected static final String ENDPOINT_RMW_BASE = "http://192.168.0.12:9000/oms/rmw";
    protected static final String SUCCESS = "Success!";

    /**
     * Function is responsible for pushing a new view
     * request to the applications request queue.
     */
    public void view(Context context, JsonReader<M> reader, String id, Consumer<M> onSuccess, Consumer<VolleyError> onFailure) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.GET(
                getViewEndpoint(id), SecurityUtils.createAuthenticationHeader(context), reader, onSuccess, onFailure
        ));
    }

    /**
     * Function is responsible for pushing a new view
     * request to the applications request queue.
     */
    public void view(Context context, JsonReader<M> reader, String id, Consumer<M> onSuccess) {
        view(context, reader, id, onSuccess, error -> {
            if (error.networkResponse != null) {
                if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Function is responsible for pushing a new list
     * request to the applications request queue.
     */
    public void list(Context context, JsonReader<M> reader, Consumer<List<M>> onSuccess) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.LIST(
                getListEndpoint(), SecurityUtils.createAuthenticationHeader(context), reader, onSuccess, error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        ));
    }

    /**
     * Function is responsible for pushing a new delete
     * request to the applications request queue.
     */
    public void remove(Context context, String id) {
        DialogUtils.buildConfirmationDialog(context, dialogInterface -> RequestQueue.getInstance(context)
                .addToRequestQueue(RequestBuilder.DELETE(getRemoveEndpoint(id), SecurityUtils.createAuthenticationHeader(context), response -> {
                    Toast.makeText(context, SUCCESS, Toast.LENGTH_LONG).show();
                    ((Activity) context).finish();
                }, error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                })));
    }

    /**
     * Function is responsible for pushing a new create
     * request to the applications request queue.
     */
    public void create(Context context, JsonWriter<M> writer, M model, List<ValidationGroup> validationGroups) {
        writer.write(model).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.POST(
                getCreateEndpoint(), json, SecurityUtils.createAuthenticationHeader(context), success -> {
                    Toast.makeText(context, SUCCESS, Toast.LENGTH_LONG).show();
                    ((Activity) context).finish();
                },
                error -> handleValidationFaults(context, error, validationGroups)
        )));
    }

    /**
     * Function is responsible for pushing a new update
     * request to the applications request queue.
     */
    public void update(Context context, JsonWriter<M> writer, String id, M model, List<ValidationGroup> validationGroups) {
        writer.write(model).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.PUT(
                getUpdateEndpoint(id), json, SecurityUtils.createAuthenticationHeader(context), success -> {
                    Toast.makeText(context, SUCCESS, Toast.LENGTH_LONG).show();
                    ((Activity) context).finish();
                },
                error -> handleValidationFaults(context, error, validationGroups)
        )));
    }

    /**
     * Function is responsible for handling validation faults
     * returned by the OMS ReactiveMiddleware application.
     */
    protected void handleValidationFaults(Context context, VolleyError error, List<ValidationGroup> validationGroups) {
        if (error.networkResponse != null) {
            if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
            } else {
                try {
                    JSONArray array = new JSONArray(new String(error.networkResponse.data));
                    List<ValidationFault> faults = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++)
                        new ValidationFaultJson().read(array.getJSONObject(i)).ifPresent(faults::add);
                    TextViewUtils.displayValidationFaults(context, validationGroups, faults);
                } catch (JSONException e) {
                    Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Function is responsible for providing the request handlers
     * view endpoint.
     */
    protected abstract String getViewEndpoint(String id);

    /**
     * Function is responsible for providing the request handlers
     * list endpoint.
     */
    protected abstract String getListEndpoint();

    /**
     * Function is responsible for providing the request handlers
     * remove endpoint.
     */
    protected abstract String getRemoveEndpoint(String id);

    /**
     * Function is responsible for providing the request handlers
     * create endpoint.
     */
    protected abstract String getCreateEndpoint();

    /**
     * Function is responsible for providing the request handlers
     * update endpoint.
     */
    protected abstract String getUpdateEndpoint(String id);
}
