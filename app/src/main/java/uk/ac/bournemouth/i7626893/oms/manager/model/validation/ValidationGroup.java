package uk.ac.bournemouth.i7626893.oms.manager.model.validation;

import android.widget.EditText;
import android.widget.TextView;

import lombok.Data;

/**
 * Data class represents a grouping of a forms
 * title, inputView and validationView.
 */
@Data
public class ValidationGroup {
    private final String title;
    private final EditText inputView;
    private final TextView validationView;
}
