package uk.ac.bournemouth.i7626893.oms.manager.activity.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.Arrays;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.activity.discountcode.DiscountCodeViewActivity;
import uk.ac.bournemouth.i7626893.oms.manager.activity.menu.MenuItemViewActivity;
import uk.ac.bournemouth.i7626893.oms.manager.activity.order.delivery.DeliveryOptionsActivity;
import uk.ac.bournemouth.i7626893.oms.manager.activity.user.UserLoginActivity;
import uk.ac.bournemouth.i7626893.oms.manager.activity.user.UserManagementActivity;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.order.OrderAdapter;
import uk.ac.bournemouth.i7626893.oms.manager.json.order.OrderJson;
import uk.ac.bournemouth.i7626893.oms.manager.request.order.OrderRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.RecyclerViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the admin view Order Activity.
 */
public class OrderViewAdminActivity extends AppCompatActivity {

    private final OrderRequestHandler orderRequestHandler;
    private final OrderJson orderJson;
    private RecyclerView recyclerView;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    public OrderViewAdminActivity() {
        this.orderRequestHandler = new OrderRequestHandler();
        this.orderJson = new OrderJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_view_admin);

        this.recyclerView = findViewById(R.id.component_recycler);
        RecyclerViewUtils.renderBorders(this, recyclerView);
        updateRecyclerView();

        this.drawerLayout = findViewById(R.id.activity_order_view_admin_drawer_layout);
        this.navigationView = findViewById(R.id.activity_order_view_admin_navbar);
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {

                case R.id.navbar_activity_order_view_po:
                    startActivity(new Intent(this, PreviousOrderViewActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_order_view_mm:
                    startActivity(new Intent(this, MenuItemViewActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_order_view_dc:
                    startActivity(new Intent(this, DiscountCodeViewActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_order_view_um:
                    startActivity(new Intent(this, UserManagementActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_order_view_do:
                    startActivity(new Intent(this, DeliveryOptionsActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_order_view_so:
                    SecurityUtils.storeAuthenticationToken(this, "");
                    Intent intent = new Intent(this, UserLoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;

                default:
                    drawerLayout.closeDrawers();
                    return true;
            }
        });

        ToolbarUtils.setToolbar(this, "Pending Orders");
        ToolbarUtils.setSideMenuBar(this);
        ToolbarUtils.setupNavbarHeader(this, navigationView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateRecyclerView();
        ToolbarUtils.setupNavbarHeader(this, navigationView);
    }

    /**
     * Function is responsible for retrieving all Orders
     * currently stored by the OMS ReactiveMiddleware application
     * and rendering the information in a RecyclerView.
     */
    private void updateRecyclerView() {
        orderRequestHandler.listOrdersByOrderStatuses(this, orderJson, Arrays.asList("ACCEPTED", "COOKING", "DELIVERY"), orders ->
                recyclerView.setAdapter(new OrderAdapter(this, orders, ((position, v) -> {
                    Intent intent = new Intent(this, OrderDetailsViewActivity.class);
                    intent.putExtra(SharedPreferenceUtils.KEY_ORDER_ID, orders.get(position).getId());
                    startActivity(intent);
                })))
        );
    }
}
