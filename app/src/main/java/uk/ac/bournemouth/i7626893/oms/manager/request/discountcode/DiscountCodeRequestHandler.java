package uk.ac.bournemouth.i7626893.oms.manager.request.discountcode;


import android.content.Context;
import android.widget.Toast;

import java.net.HttpURLConnection;

import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.discountcode.DiscountCode;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestQueue;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.RequestBuilder;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for handling all DiscountCode
 * related HTTP requests.
 */
public class DiscountCodeRequestHandler extends RequestHandler<DiscountCode> {
    @Override
    protected String getViewEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/discount-code/" + id + "/view";
    }

    @Override
    protected String getListEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/discount-code/list";
    }

    @Override
    protected String getRemoveEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/discount-code/" + id + "/delete";
    }

    @Override
    protected String getCreateEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/discount-code";
    }

    @Override
    protected String getUpdateEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/discount-code/" + id + "/update";
    }

    private String getSendDiscountEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/discount-code/" + id + "/send";
    }

    /**
     * Function is responsible for pushing a new send discount code
     * request to the applications request queue.
     */
    public void sendDiscountCode(Context context, String id) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.POST(
                getSendDiscountEndpoint(id), null, SecurityUtils.createAuthenticationHeader(context),
                success -> Toast.makeText(context, SUCCESS, Toast.LENGTH_LONG).show(),
                error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        ));
    }
}
