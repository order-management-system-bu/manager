package uk.ac.bournemouth.i7626893.oms.manager.utility.android;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.util.function.Consumer;

/**
 * Utility class is responsible for providing a range of
 * Dialog-related utility functions.
 */
public class DialogUtils {

    /**
     * Function is responsible for creating a closable confirmation
     * popup.
     */
    public static void buildConfirmationDialog(Context context, Consumer<DialogInterface> onConfirm) {
        new AlertDialog.Builder(context)
                .setTitle("Are you sure?")
                .setMessage("This potentially harmful operation cannot be undone. Are you sure you want to continue?")
                .setNegativeButton("Cancel", ((dialog, which) -> dialog.dismiss()))
                .setPositiveButton("Confirm", ((dialog, which) -> onConfirm.accept(dialog)))
                .create()
                .show();
    }
}
