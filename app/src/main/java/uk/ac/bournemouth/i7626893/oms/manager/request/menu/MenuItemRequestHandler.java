package uk.ac.bournemouth.i7626893.oms.manager.request.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import uk.ac.bournemouth.i7626893.oms.manager.activity.menu.MenuItemModificationSelectionActivity;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuItem;
import uk.ac.bournemouth.i7626893.oms.manager.model.validation.ValidationGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestQueue;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.RequestBuilder;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for handling all MenuItem
 * related HTTP requests.
 */
public class MenuItemRequestHandler extends RequestHandler<MenuItem> {
    @Override
    protected String getViewEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item/" + id + "/view";
    }

    @Override
    protected String getListEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/menu-item/list";
    }

    @Override
    protected String getRemoveEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item/" + id + "/delete";
    }

    @Override
    protected String getCreateEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/menu-item";
    }

    @Override
    protected String getUpdateEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item/" + id + "/update";
    }

    private String getListByMenuCategoryEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item/" + id + "/list/menu_category";
    }

    @Override
    public void create(Context context, JsonWriter<MenuItem> writer, MenuItem model, List<ValidationGroup> validationGroups) {
        writer.write(model).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.POST(
                getCreateEndpoint(), json, SecurityUtils.createAuthenticationHeader(context), success -> {
                    ((Activity) context).finish();
                    Intent intent = new Intent(context, MenuItemModificationSelectionActivity.class);
                    intent.putExtra(SharedPreferenceUtils.KEY_MENU_ITEM_ID, JsonUtils.stringOrNull(success, "data"));
                    context.startActivity(intent);
                },
                error -> handleValidationFaults(context, error, validationGroups)
        )));
    }

    @Override
    public void update(Context context, JsonWriter<MenuItem> writer, String id, MenuItem model, List<ValidationGroup> validationGroups) {
        writer.write(model).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.PUT(
                getUpdateEndpoint(id), json, SecurityUtils.createAuthenticationHeader(context), success -> {
                    ((Activity) context).finish();
                    Intent intent = new Intent(context, MenuItemModificationSelectionActivity.class);
                    intent.putExtra(SharedPreferenceUtils.KEY_MENU_ITEM_ID, id);
                    context.startActivity(intent);
                },
                error -> handleValidationFaults(context, error, validationGroups)
        )));
    }

    /**
     * Function is responsible for pushing a new update request
     * to the applications request queue.
     */
    public void updateSelections(Context context, JsonWriter<MenuItem> writer, String id, MenuItem model) {
        super.update(context, writer, id, model, Collections.emptyList());
    }

    /**
     * Function is responsible for pushing a new list by
     * MenuCategory request to the applications request
     * queue.
     */
    public void listByMenuCategory(Context context, JsonReader<MenuItem> reader, String id, Consumer<List<MenuItem>> onSuccess) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.LIST(
                getListByMenuCategoryEndpoint(id), SecurityUtils.createAuthenticationHeader(context), reader, onSuccess, error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        ));
    }
}
