package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import lombok.AllArgsConstructor;
import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOption;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary functionality
 * for the create MenuOption Activity.
 */
public class MenuOptionCreateActivity extends AppCompatActivity {

    private final MenuOptionRequestHandler menuOptionRequestHandler;
    private final MenuOptionJson menuOptionJson;

    public MenuOptionCreateActivity() {
        this.menuOptionRequestHandler = new MenuOptionRequestHandler();
        this.menuOptionJson = new MenuOptionJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_option_create);

        ToolbarUtils.setToolbar(this, "Create a Menu Option");

        final EditText titleView = findViewById(R.id.form_menu_item_option_input_title);
        final EditText priceView = findViewById(R.id.form_menu_item_option_input_price);

        findViewById(R.id.component_button_submit)
                .setOnClickListener(v -> menuOptionRequestHandler
                        .create(this, menuOptionJson, new MenuOption(
                                null,
                                TextViewUtils.stringOrNull(titleView),
                                TextViewUtils.doubleOrNull(priceView)
                        ), ValidationGroupUtils.createMenuOptionVg(
                                this, titleView, priceView
                        )));
    }
}
