package uk.ac.bournemouth.i7626893.oms.manager.json.order;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order.DeliveryOption;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;

/**
 * Class is responsible for providing the JSON conversion
 * logic for all instances of DeliveryOption.
 */
public class DeliveryOptionJson implements JsonReader<DeliveryOption>, JsonWriter<DeliveryOption> {
    @Override
    public Optional<DeliveryOption> read(JSONObject object) {
        return Optional.of(new DeliveryOption(
                JsonUtils.stringOrNull(object, "id"),
                JsonUtils.doubleOrNull(object, "deliveryCharge"),
                JsonUtils.doubleOrNull(object, "minimumForDelivery")
        ));
    }

    @Override
    public Optional<JSONObject> write(DeliveryOption model) {
        try {
            JSONObject json = new JSONObject();
            JsonUtils.putIfNotNull(json, "deliveryCharge", model.getDeliveryCharge());
            JsonUtils.putIfNotNull(json, "minimumForDelivery", model.getMinimumForDelivery());
            return Optional.of(json);
        } catch (JSONException e) {
            return Optional.empty();
        }
    }
}
