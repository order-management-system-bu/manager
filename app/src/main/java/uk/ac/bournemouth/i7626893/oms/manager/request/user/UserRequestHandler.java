package uk.ac.bournemouth.i7626893.oms.manager.request.user;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.function.Consumer;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user.User;
import uk.ac.bournemouth.i7626893.oms.manager.model.validation.ValidationGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestQueue;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.RequestBuilder;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for handling all User
 * related HTTP requests.
 */
public class UserRequestHandler extends RequestHandler<User> {
    @Override
    protected String getViewEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/urm/manager/" + id + "/view";
    }

    @Override
    protected String getListEndpoint() {
        return ENDPOINT_RMW_BASE + "/urm/manager/list";
    }

    @Override
    protected String getRemoveEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/urm/manager/" + id + "/delete";
    }

    @Override
    protected String getCreateEndpoint() {
        throw new RuntimeException("Creating users is not permitted.");
    }

    @Override
    protected String getUpdateEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/urm/user/" + id + "/update";
    }

    private String getAdminUpdateEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/urm/user/" + id + "/admin/update";
    }

    private String getCreateUserEndpoint() {
        return ENDPOINT_RMW_BASE + "/urm/manager";
    }

    private String getAuthenticateUserEndpoint() {
        return ENDPOINT_RMW_BASE + "/urm/user/authenticate";
    }

    private String getViewUserByAuthTokenEndpoint() {
        return ENDPOINT_RMW_BASE + "/urm/manager/view";
    }

    private String getFirstTimeLoginEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/urm/manager/" + id + "/first-time-login";
    }

    private String getResetAuthenticationToken() {
        return ENDPOINT_RMW_BASE + "/urm/user/authenticate/reset";
    }

    private String getListDeliveryDrivers() {
        return ENDPOINT_RMW_BASE + "/urm/manager/list/delivery-driver";
    }

    @Override
    public void update(Context context, JsonWriter<User> writer, String id, User model, List<ValidationGroup> validationGroups) {
        writer.write(model).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.PUT(
                getUpdateEndpoint(id), json, SecurityUtils.createAuthenticationHeader(context), success -> {
                    SharedPreferenceUtils.storeManagerPreferences(context, id, model.getForename() + " " + model.getSurname());
                    Toast.makeText(context, SUCCESS, Toast.LENGTH_LONG).show();
                    ((Activity) context).finish();
                },
                error -> handleValidationFaults(context, error, validationGroups)
        )));
    }

    /**
     * Function is responsible for pushing a new update admin fields
     * request to the applications request queue.
     */
    public void adminUpdate(Context context, JsonWriter<User> writer, String id, User model) {
        writer.write(model).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.PUT(
                getAdminUpdateEndpoint(id), json, SecurityUtils.createAuthenticationHeader(context), success -> {
                    Toast.makeText(context, SUCCESS, Toast.LENGTH_LONG).show();
                    ((Activity) context).finish();
                },
                error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        )));
    }

    /**
     * Function is responsible for pushing a new create
     * non admin user request to the applications request
     * queue.
     */
    public void createUser(Context context, JsonWriter<User> writer, User model, List<ValidationGroup> validationGroups) {
        writer.write(model).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.POST(
                getCreateUserEndpoint(), json, SecurityUtils.createAuthenticationHeader(context), success -> {
                    Toast.makeText(context, SUCCESS, Toast.LENGTH_LONG).show();
                    ((Activity) context).finish();
                },
                error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else if (error.networkResponse.statusCode == HttpURLConnection.HTTP_BAD_REQUEST) {
                            handleValidationFaults(context, error, validationGroups);
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        )));
    }

    /**
     * Function is responsible for pushing a new authenticate
     * user request to the applications request queue.
     */
    public void authenticateUser(
            Context context,
            JsonWriter<User> writer,
            User model,
            Consumer<JSONObject> onSuccess,
            Consumer<VolleyError> onFailure
    ) {
        writer.write(model).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.POST(
                getAuthenticateUserEndpoint(), json, null, onSuccess, onFailure)));
    }

    /**
     * Function is responsible for pushing a new first time login
     * request to the applications request queue.
     */
    public void firstTimeLogin(
            Context context,
            JsonWriter<User> writer,
            String userId,
            User model,
            List<ValidationGroup> validationGroups,
            Consumer<JSONObject> onSuccess
    ) {
        writer.write(model).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.PUT(
                getFirstTimeLoginEndpoint(userId), json, SecurityUtils.createAuthenticationHeader(context), onSuccess,
                error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else if (error.networkResponse.statusCode == HttpURLConnection.HTTP_BAD_REQUEST) {
                            handleValidationFaults(context, error, validationGroups);
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        )));
    }

    /**
     * Function is responsible for pushing a update authentication token
     * request the the applications request queue.
     */
    public void resetAuthenticationToken(Context context, Consumer<String> onSuccess, Consumer<VolleyError> onFailure) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.POST(
                getResetAuthenticationToken(),
                null,
                SecurityUtils.createAuthenticationHeader(context),
                object -> onSuccess.accept(JsonUtils.stringOrNull(object, "data")),
                onFailure
        ));
    }

    /**
     * Function is responsible for pushing a new user retrieval based
     * on authentication token request to the applications request
     * queue.
     */
    public void viewUserByAuthToken(Context context, JsonReader<User> reader, Consumer<User> onSuccess, Consumer<VolleyError> onFailure) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.GET(
                getViewUserByAuthTokenEndpoint(), SecurityUtils.createAuthenticationHeader(context), reader, onSuccess, onFailure));
    }

    /**
     * Function is responsible for pushing a new list delivery drivers
     * request to the applications request queue.
     */
    public void listDeliveryDrivers(Context context, JsonReader<User> reader, Consumer<List<User>> onSuccess) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.LIST(
                getListDeliveryDrivers(), SecurityUtils.createAuthenticationHeader(context), reader, onSuccess, error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        ));
    }
}
