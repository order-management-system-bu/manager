package uk.ac.bournemouth.i7626893.oms.manager.model.rmw.discountcode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;
import uk.ac.bournemouth.i7626893.oms.manager.model.ModelWithTitle;

/**
 * Model class represents a single DiscountCode
 * associated with the OMS ReactiveMiddleware
 * application.
 */
@AllArgsConstructor
@Getter
public class DiscountCode implements ModelWithTitle {
    private final String id;
    private final String title;
    private final Double discount;
    private final String discountType;
    private final Double spendEligibility;
    private final Boolean enabled;
    private final String code;

    public DiscountCode(
            String title,
            Double discount,
            String discountType,
            Double spendEligibility,
            Boolean enabled
    ) {
        this.title = title;
        this.discount = discount;
        this.discountType = discountType;
        this.spendEligibility = spendEligibility;
        this.enabled = enabled;

        this.id = null;
        this.code = null;
    }

    public DiscountCode(Boolean enabled) {
        this.enabled = enabled;

        this.id = null;
        this.title = null;
        this.discount = null;
        this.discountType = null;
        this.spendEligibility = null;
        this.code = null;
    }
}
