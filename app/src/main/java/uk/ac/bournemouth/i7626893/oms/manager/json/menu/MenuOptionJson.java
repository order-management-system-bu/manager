package uk.ac.bournemouth.i7626893.oms.manager.json.menu;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOption;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;

/**
 * Class is responsible for providing the JSON conversion
 * logic for all instances of MenuOption.
 */
public class MenuOptionJson implements JsonReader<MenuOption>, JsonWriter<MenuOption> {
    @Override
    public Optional<MenuOption> read(JSONObject object) {
        return Optional.of(new MenuOption(
                JsonUtils.stringOrNull(object, "id"),
                JsonUtils.stringOrNull(object, "title"),
                JsonUtils.doubleOrNull(object, "price")
        ));
    }

    @Override
    public Optional<JSONObject> write(MenuOption model) {
        try {
            JSONObject object = new JSONObject();
            JsonUtils.putIfNotNull(object, "title", model.getTitle());
            JsonUtils.putIfNotNull(object, "price", model.getPrice());
            return Optional.of(object);
        } catch (JSONException e) {
            return Optional.empty();
        }
    }
}
