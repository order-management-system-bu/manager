package uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order;

import java.util.List;

import lombok.Data;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;

/**
 * Model class represents a single OrderOption
 * associated with the OMS ReactiveMiddleware
 * application.
 */
@Data
public class OrderOption implements Model {
    private final String menuItemId;
    private final List<String> selectedOptionIds;
}
