package uk.ac.bournemouth.i7626893.oms.manager.model;

/**
 * Interface is responsible for associating class
 * with a Model that has a title field.
 */
public interface ModelWithTitle extends Model {
    String getTitle();
}
