package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import lombok.AllArgsConstructor;
import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuCategoryJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuCategory;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuCategoryRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the create MenuCategory Activity.
 */
public class MenuCategoryCreateActivity extends AppCompatActivity {

    private final MenuCategoryRequestHandler menuCategoryRequestHandler;
    private final MenuCategoryJson menuCategoryJson;

    public MenuCategoryCreateActivity() {
        this.menuCategoryRequestHandler = new MenuCategoryRequestHandler();
        this.menuCategoryJson = new MenuCategoryJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_category_create);

        ToolbarUtils.setToolbar(this, "Create a Menu Category");

        final EditText titleView = findViewById(R.id.form_menu_category_input_title);
        final EditText descriptionView = findViewById(R.id.form_menu_category_input_description);

        findViewById(R.id.component_button_submit)
                .setOnClickListener(v -> menuCategoryRequestHandler
                        .create(this, menuCategoryJson, new MenuCategory(
                                null,
                                TextViewUtils.stringOrNull(titleView),
                                TextViewUtils.stringOrNull(descriptionView)
                        ), ValidationGroupUtils.createMenuCategoryVg(
                                this, titleView, descriptionView
                        )));
    }
}
