package uk.ac.bournemouth.i7626893.oms.manager.utility.security;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.R;

/**
 * Utility class is responsible for providing a range
 * of security-related functions.
 */
public final class SecurityUtils {

    private static final String AUTH_TOKEN_KEY = "oms.manager.auth.token";

    /**
     * Function is responsible for storing an authentication token
     * in androids SharedPreferences.
     */
    public static void storeAuthenticationToken(Context context, String authToken) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(AUTH_TOKEN_KEY, authToken).commit();
    }

    /**
     * Function is responsible for retrieving an authentication token
     * from androids SharedPreferences.
     */
    public static Optional<String> retrieveAuthenticationToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        final String authenticationToken = sharedPreferences.getString(AUTH_TOKEN_KEY, null);
        return Optional.ofNullable(authenticationToken);
    }

    /**
     * Function is responsible for constructing the authorization header
     * to be included in HTTP requests to OMS ReactiveMiddleware.
     */
    public static Map<String, String> createAuthenticationHeader(Context context) {
        return Collections.singletonMap("Authorization", retrieveAuthenticationToken(context).orElse(null));
    }
}
