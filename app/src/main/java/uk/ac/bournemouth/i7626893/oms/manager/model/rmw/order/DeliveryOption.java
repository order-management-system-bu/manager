package uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order;

import lombok.Data;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;

/**
 * Model class represents a single DeliveryOption
 * associated with the OMS ReactiveMiddleware
 * application.
 */
@Data
public class DeliveryOption implements Model {
    private final String id;
    private final Double deliveryCharge;
    private final Double minimumForDelivery;
}
