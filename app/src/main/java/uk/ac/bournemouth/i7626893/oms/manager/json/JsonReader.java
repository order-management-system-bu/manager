package uk.ac.bournemouth.i7626893.oms.manager.json;

import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.model.Model;

/**
 * Interface represents a JSON reader class.
 */
public interface JsonReader<M extends Model> {
    /**
     * Function is responsible for reading a JSONObject
     * and constructing a new instance of M from it.
     */
    Optional<M> read(JSONObject object);
}
