package uk.ac.bournemouth.i7626893.oms.manager.json.validation;

import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.validation.ValidationFault;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;

/**
 * Class is responsible for providing the JSON conversion
 * logic for all instances of ValidationFault.
 */
public class ValidationFaultJson implements JsonReader<ValidationFault> {
    @Override
    public Optional<ValidationFault> read(JSONObject object) {
        return Optional.of(new ValidationFault(
                JsonUtils.stringOrNull(object, "field"),
                JsonUtils.stringOrNull(object, "fault")
        ));
    }
}
