package uk.ac.bournemouth.i7626893.oms.manager.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import java.util.Collections;
import java.util.List;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.user.UserJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user.User;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.validation.ValidationFault;
import uk.ac.bournemouth.i7626893.oms.manager.model.validation.ValidationGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.user.UserRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the user first time login Activity.
 */
public class UserLoginFirstTimeActivity extends AppCompatActivity {

    private final UserRequestHandler userRequestHandler;
    private final UserJson userJson;

    public UserLoginFirstTimeActivity() {
        this.userRequestHandler = new UserRequestHandler();
        this.userJson = new UserJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login_first_time);

        ToolbarUtils.setToolbar(this, "Welcome to OMS Manager");

        SharedPreferenceUtils.getManagerId(this).ifPresent(managerId -> {
            final EditText forenameView = findViewById(R.id.form_user_login_first_time_input_forename);
            final EditText surnameView = findViewById(R.id.form_user_login_first_time_input_surname);
            final EditText passwordView = findViewById(R.id.form_user_login_first_time_input_password);
            final EditText confirmPasswordView = findViewById(R.id.form_user_login_first_time_input_confirm_password);

            final List<ValidationGroup> validationGroups = ValidationGroupUtils.createUserVg(
                    this, forenameView, surnameView, passwordView, confirmPasswordView);

            findViewById(R.id.component_button_submit).setOnClickListener(v -> {
                if (!passwordView.getText().toString().equals(confirmPasswordView.getText().toString())) {
                    TextViewUtils.displayValidationFaults(this, validationGroups, Collections.singletonList(
                            new ValidationFault("confirmPassword", "Passwords do not match.")
                    ));
                } else {
                    userRequestHandler.firstTimeLogin(this, userJson, managerId, new User(
                            TextViewUtils.stringOrNull(forenameView),
                            TextViewUtils.stringOrNull(surnameView),
                            TextViewUtils.stringOrNull(passwordView)
                    ), validationGroups, response -> {
                        finish();
                        startActivity(new Intent(this, UserLoginActivity.class));
                    });
                }
            });
        });
    }
}
