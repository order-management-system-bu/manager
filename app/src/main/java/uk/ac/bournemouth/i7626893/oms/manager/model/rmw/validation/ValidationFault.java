package uk.ac.bournemouth.i7626893.oms.manager.model.rmw.validation;

import lombok.Data;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;

/**
 * Data class represents a validation failure
 * associated with the OMS ReactiveMiddleware
 * application.
 */
@Data
public class ValidationFault implements Model {
    private final String field;
    private final String fault;
}
