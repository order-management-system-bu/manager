package uk.ac.bournemouth.i7626893.oms.manager.activity.order.delivery;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.order.DeliveryOptionJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order.DeliveryOption;
import uk.ac.bournemouth.i7626893.oms.manager.request.order.DeliveryOptionRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the delivery options Activity.
 */
public class DeliveryOptionsActivity extends AppCompatActivity {

    private final DeliveryOptionRequestHandler deliveryOptionRequestHandler;
    private final DeliveryOptionJson deliveryOptionJson;

    public DeliveryOptionsActivity() {
        this.deliveryOptionRequestHandler = new DeliveryOptionRequestHandler();
        this.deliveryOptionJson = new DeliveryOptionJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_options);

        ToolbarUtils.setToolbar(this, "Update Delivery Options");

        final EditText deliveryChargeView = findViewById(R.id.form_delivery_options_input_delivery_charge);
        final EditText minimumDeliveryView = findViewById(R.id.form_delivery_options_input_minimum_delivery);

        deliveryOptionRequestHandler.view(this, deliveryOptionJson, null, deliveryOption -> {
            deliveryChargeView.setText(String.format("%.02f", deliveryOption.getDeliveryCharge()));
            minimumDeliveryView.setText(String.format("%.02f", deliveryOption.getMinimumForDelivery()));
        });

        findViewById(R.id.component_button_submit).setOnClickListener(v ->
                deliveryOptionRequestHandler.update(this, deliveryOptionJson, null, new DeliveryOption(
                        null,
                        TextViewUtils.doubleOrNull(deliveryChargeView),
                        TextViewUtils.doubleOrNull(minimumDeliveryView)
                ), ValidationGroupUtils.createDeliveryOptionVg(
                        this, deliveryChargeView, minimumDeliveryView
                ))
        );
    }
}
