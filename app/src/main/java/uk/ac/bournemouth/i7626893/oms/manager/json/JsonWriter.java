package uk.ac.bournemouth.i7626893.oms.manager.json;

import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.model.Model;

/**
 * Interface represents a JSON writer class.
 */
public interface JsonWriter<M extends Model> {
    Optional<JSONObject> write(M model);
}
