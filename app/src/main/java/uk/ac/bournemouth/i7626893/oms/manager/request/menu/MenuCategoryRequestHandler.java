package uk.ac.bournemouth.i7626893.oms.manager.request.menu;

import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuCategory;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestHandler;

/**
 * Class is responsible for handling all MenuCategory
 * related HTTP requests.
 */
public class MenuCategoryRequestHandler extends RequestHandler<MenuCategory> {
    @Override
    protected String getViewEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-category/" + id + "/view";
    }

    @Override
    protected String getListEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/menu-category/list";
    }

    @Override
    protected String getRemoveEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-category/" + id + "/delete";
    }

    @Override
    protected String getCreateEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/menu-category/";
    }

    @Override
    protected String getUpdateEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-category/" + id + "/update";
    }
}
