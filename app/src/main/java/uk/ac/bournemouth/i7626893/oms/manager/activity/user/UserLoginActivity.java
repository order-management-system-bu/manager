package uk.ac.bournemouth.i7626893.oms.manager.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.activity.order.OrderViewActivity;
import uk.ac.bournemouth.i7626893.oms.manager.activity.order.OrderViewAdminActivity;
import uk.ac.bournemouth.i7626893.oms.manager.activity.order.delivery.OrderViewDeliveryPendingActivity;
import uk.ac.bournemouth.i7626893.oms.manager.json.user.UserJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user.User;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.validation.ValidationFault;
import uk.ac.bournemouth.i7626893.oms.manager.model.validation.ValidationGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.user.UserRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the user login Activity.
 */
public class UserLoginActivity extends AppCompatActivity {

    private final UserRequestHandler userRequestHandler;
    private final UserJson userJson;

    public UserLoginActivity() {
        this.userRequestHandler = new UserRequestHandler();
        this.userJson = new UserJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        ToolbarUtils.setToolbar(this, "Sign In");

        final EditText usernameView = findViewById(R.id.form_user_login_input_username);
        final EditText passwordView = findViewById(R.id.form_user_login_input_password);

        final List<ValidationGroup> validationGroups = Arrays.asList(
                new ValidationGroup("username", usernameView, findViewById(R.id.form_user_login_validation_username)),
                new ValidationGroup("password", passwordView, findViewById(R.id.form_user_login_validation_password))
        );

        userRequestHandler.viewUserByAuthToken(this, userJson, user -> userRequestHandler.resetAuthenticationToken(this, token -> {
                    final String fullName = user.getForename() + " " + user.getSurname();
                    SecurityUtils.storeAuthenticationToken(this, token);
                    SharedPreferenceUtils.storeManagerPreferences(this, user.getId(), fullName);
                    finish();
                    if (user.getFirstTimeLogin())
                        startActivity(new Intent(this, UserLoginFirstTimeActivity.class));
                    else if (user.getAdmin())
                        startActivity(new Intent(this, OrderViewAdminActivity.class));
                    else if (user.getDeliveryDriver())
                        startActivity(new Intent(this, OrderViewDeliveryPendingActivity.class));
                    else
                        startActivity(new Intent(this, OrderViewActivity.class));
                }, error -> setupLoginButton(usernameView, passwordView, validationGroups)
        ), error -> setupLoginButton(usernameView, passwordView, validationGroups));
    }

    private void setupLoginButton(EditText usernameView, EditText passwordView, List<ValidationGroup> validationGroups) {
        findViewById(R.id.form_user_login_button_login).setOnClickListener(v -> userRequestHandler.authenticateUser(
                this, userJson, new User(TextViewUtils.stringOrNull(usernameView), TextViewUtils.stringOrNull(passwordView)), response -> {
                    SecurityUtils.storeAuthenticationToken(this, JsonUtils.stringOrNull(response, "data"));
                    userRequestHandler.viewUserByAuthToken(this, userJson, user -> {
                        final String fullName = user.getForename() + " " + user.getSurname();
                        SharedPreferenceUtils.storeManagerPreferences(this, user.getId(), fullName);
                        finish();
                        if (user.getFirstTimeLogin())
                            startActivity(new Intent(this, UserLoginFirstTimeActivity.class));
                        else if (user.getAdmin())
                            startActivity(new Intent(this, OrderViewAdminActivity.class));
                        else if (user.getDeliveryDriver())
                            startActivity(new Intent(this, OrderViewDeliveryPendingActivity.class));
                        else
                            startActivity(new Intent(this, OrderViewActivity.class));
                    }, error -> {
                        if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED ||
                                    error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                                Toast.makeText(this, RequestHandler.UNAUTHORIZED, Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(this, RequestHandler.FAILURE, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(this, RequestHandler.FAILURE, Toast.LENGTH_LONG).show();
                        }
                    });
                }, error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND)
                            TextViewUtils.displayValidationFaults(this, validationGroups, Collections.singletonList(new ValidationFault(
                                    "username", "An account with this username could not be found."
                            )));
                        else if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED)
                            TextViewUtils.displayValidationFaults(this, validationGroups, Collections.singletonList(new ValidationFault(
                                    "username", "Failed to authenticate the user."
                            )));
                        else if (error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN)
                            TextViewUtils.displayValidationFaults(this, validationGroups, Collections.singletonList(new ValidationFault(
                                    "username", "The account associated with this email is not active. Please contact your system administrator for more information."
                            )));
                        else
                            Toast.makeText(this, RequestHandler.FAILURE, Toast.LENGTH_LONG).show();
                    }
                }));
    }
}
