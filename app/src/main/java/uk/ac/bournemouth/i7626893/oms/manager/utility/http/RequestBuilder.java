package uk.ac.bournemouth.i7626893.oms.manager.utility.http;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;

/**
 * Utility class is responsible for providing a range of
 * request building functions.
 */
public class RequestBuilder {
    public static <M extends Model> JsonObjectRequest GET(
            String endpoint,
            Map<String, String> headers,
            JsonReader<M> reader,
            Consumer<M> onSuccess,
            Consumer<VolleyError> onFailure
    ) {
        return new JsonObjectRequest(Request.Method.GET, endpoint, null,
                response -> reader.read(response).ifPresent(onSuccess), onFailure::accept) {
            @Override
            public Map<String, String> getHeaders() {
                return headers == null ? Collections.emptyMap() : headers;
            }
        };
    }

    /**
     * Function is responsible for constructing a new
     * LIST request.
     */
    public static <M extends Model> JsonArrayRequest LIST(
            String endpoint,
            Map<String, String> headers,
            JsonReader<M> reader,
            Consumer<List<M>> onSuccess,
            Consumer<VolleyError> onFailure
    ) {
        return new JsonArrayRequest(endpoint, response -> {
            try {
                final List<M> models = new ArrayList<>();
                for (int i = 0; i < response.length(); i++)
                    reader.read(response.getJSONObject(i)).ifPresent(models::add);
                onSuccess.accept(models);
            } catch (JSONException e) {
                onSuccess.accept(Collections.emptyList());
            }
        }, onFailure::accept) {
            @Override
            public Map<String, String> getHeaders() {
                return headers == null ? Collections.emptyMap() : headers;
            }
        };
    }

    /**
     * Function is responsible for constructing a new
     * DELETE request.
     */
    public static JsonObjectRequest DELETE(
            String endpoint,
            Map<String, String> headers,
            Consumer<JSONObject> onSuccess,
            Consumer<VolleyError> onFailure
    ) {
        return new JsonObjectRequest(Request.Method.DELETE, endpoint, null, onSuccess::accept, onFailure::accept) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse networkResponse) {
                try {
                    JSONObject response = new JSONObject();
                    try {
                        response.put("data", new String(networkResponse.data));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return Response.success(response, HttpHeaderParser.parseCacheHeaders(networkResponse));
                } catch (JsonSyntaxException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            public Map<String, String> getHeaders() {
                return headers == null ? Collections.emptyMap() : headers;
            }
        };
    }

    /**
     * Function is responsible for constructing a new
     * POST request.
     */
    public static JsonObjectRequest POST(
            String endpoint,
            JSONObject json,
            Map<String, String> headers,
            Consumer<JSONObject> onSuccess,
            Consumer<VolleyError> onFailure
    ) {
        return new JsonObjectRequest(Request.Method.POST, endpoint, json, onSuccess::accept, onFailure::accept) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse networkResponse) {
                try {
                    JSONObject response = new JSONObject();
                    try {
                        response.put("data", new String(networkResponse.data));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return Response.success(response, HttpHeaderParser.parseCacheHeaders(networkResponse));
                } catch (JsonSyntaxException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            public Map<String, String> getHeaders() {
                return headers == null ? Collections.emptyMap() : headers;
            }
        };
    }

    /**
     * Function is responsible for constructing a new
     * PUT request.
     */
    public static JsonObjectRequest PUT(
            String endpoint,
            JSONObject json,
            Map<String, String> headers,
            Consumer<JSONObject> onSuccess,
            Consumer<VolleyError> onFailure
    ) {
        return new JsonObjectRequest(Request.Method.PUT, endpoint, json, onSuccess::accept, onFailure::accept) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse networkResponse) {
                JSONObject response = new JSONObject();
                try {
                    response.put("data", new String(networkResponse.data));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return Response.success(response, HttpHeaderParser.parseCacheHeaders(networkResponse));
            }

            @Override
            public Map<String, String> getHeaders() {
                return headers == null ? Collections.emptyMap() : headers;
            }
        };
    }
}
