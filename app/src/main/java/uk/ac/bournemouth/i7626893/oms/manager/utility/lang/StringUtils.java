package uk.ac.bournemouth.i7626893.oms.manager.utility.lang;

/**
 * Utility class is responsible for providing a range
 * of String-related utility functions.
 */
public class StringUtils {
    /**
     * Function is responsible for evaluating a given String
     * and returning an empty String if null.
     */
    public static String stringOrBlank(String s) {
        return s == null || s.equals("null") ? "" : s;
    }
}
