package uk.ac.bournemouth.i7626893.oms.manager.activity.discountcode;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.activity.user.UserLoginActivity;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.GenericAdapter;
import uk.ac.bournemouth.i7626893.oms.manager.json.discountcode.DiscountCodeJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.discountcode.DiscountCode;
import uk.ac.bournemouth.i7626893.oms.manager.request.discountcode.DiscountCodeRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.RecyclerViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the view DiscountCode Activity.
 */
public class DiscountCodeViewActivity extends AppCompatActivity {

    private final DiscountCodeRequestHandler discountCodeRequestHandler;
    private final DiscountCodeJson discountCodeJson;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private RecyclerView recyclerView;

    public DiscountCodeViewActivity() {
        this.discountCodeRequestHandler = new DiscountCodeRequestHandler();
        this.discountCodeJson = new DiscountCodeJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount_code_view);

        this.drawerLayout = findViewById(R.id.activity_discount_code_view_drawer_layout);
        this.navigationView = findViewById(R.id.activity_discount_code_view_navbar);
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.navbar_activity_discount_code_sdc:
                    startActivity(new Intent(this, DiscountCodeSendActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_discount_code_so:
                    SecurityUtils.storeAuthenticationToken(this, "");
                    Intent intent = new Intent(this, UserLoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;

                default:
                    drawerLayout.closeDrawers();
                    return true;
            }
        });

        ToolbarUtils.setToolbar(this, "Discount Codes");
        ToolbarUtils.setSideMenuBar(this);
        ToolbarUtils.setupNavbarHeader(this, navigationView);

        this.recyclerView = findViewById(R.id.component_recycler);
        RecyclerViewUtils.renderBorders(this, recyclerView);
        updateRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateRecyclerView();
        ToolbarUtils.setupNavbarHeader(this, navigationView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_view, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.appbar_generic_create:
                startActivity(new Intent(this, DiscountCodeCreateActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Function is responsible for retrieving all DiscountCodes
     * currently stored by the OMS ReactiveMiddleware application
     * and rendering the information in a RecyclerView.
     */
    private void updateRecyclerView() {
        discountCodeRequestHandler.list(this, discountCodeJson, discountCodes ->
                recyclerView.setAdapter(new GenericAdapter<DiscountCode>(this, discountCodes, (position, v) -> {
                    Intent intent = new Intent(this, DiscountCodeViewDetailsActivity.class);
                    intent.putExtra(SharedPreferenceUtils.KEY_DISCOUNT_CODE_ID, discountCodes.get(position).getId());
                    startActivity(intent);
                }))
        );
    }
}
