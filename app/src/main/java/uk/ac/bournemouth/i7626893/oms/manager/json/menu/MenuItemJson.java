package uk.ac.bournemouth.i7626893.oms.manager.json.menu;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuItem;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;

/**
 * Class is responsible for providing the JSON conversion
 * logic for all instances of MenuItem.
 */
public class MenuItemJson implements JsonReader<MenuItem>, JsonWriter<MenuItem> {
    @Override
    public Optional<MenuItem> read(JSONObject object) {
        return Optional.of(new MenuItem(
                JsonUtils.stringOrNull(object, "id"),
                JsonUtils.stringOrNull(object, "menuCategoryId"),
                JsonUtils.stringOrNull(object, "title"),
                JsonUtils.stringOrNull(object, "description"),
                JsonUtils.doubleOrNull(object, "price"),
                JsonUtils.stringListOrEmpty(object, "additionalOptionIds"),
                JsonUtils.stringListOrEmpty(object, "additionalOptionGroupIds")
        ));
    }

    @Override
    public Optional<JSONObject> write(MenuItem model) {
        try {
            JSONObject json = new JSONObject();
            JsonUtils.putIfNotNull(json, "menuCategoryId", model.getMenuCategoryId());
            JsonUtils.putIfNotNull(json, "title", model.getTitle());
            JsonUtils.putIfNotNull(json, "description", model.getDescription());
            JsonUtils.putIfNotNull(json, "price", model.getPrice());
            JsonUtils.putIfNotNull(json, "additionalOptionIds", model.getAdditionalOptionIds());
            JsonUtils.putIfNotNull(json, "additionalOptionGroupIds", model.getAdditionalOptionGroupIds());
            return Optional.of(json);
        } catch (JSONException e) {
            return Optional.empty();
        }
    }
}
