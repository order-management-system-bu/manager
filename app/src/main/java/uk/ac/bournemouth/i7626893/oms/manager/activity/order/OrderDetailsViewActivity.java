package uk.ac.bournemouth.i7626893.oms.manager.activity.order;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuItemJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.order.OrderJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.user.UserJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order.Order;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuItemRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.order.OrderRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.user.UserRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.StringUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the view order details Activity.
 */
public class OrderDetailsViewActivity extends AppCompatActivity {

    private final OrderRequestHandler orderRequestHandler;
    private final MenuItemRequestHandler menuItemRequestHandler;
    private final MenuOptionRequestHandler menuOptionRequestHandler;
    private final UserRequestHandler userRequestHandler;
    private final OrderJson orderJson;
    private final MenuItemJson menuItemJson;
    private final MenuOptionJson menuOptionJson;
    private final UserJson userJson;
    private String deliveryDriver;

    public OrderDetailsViewActivity() {
        this.orderRequestHandler = new OrderRequestHandler();
        this.menuItemRequestHandler = new MenuItemRequestHandler();
        this.menuOptionRequestHandler = new MenuOptionRequestHandler();
        this.userRequestHandler = new UserRequestHandler();
        this.orderJson = new OrderJson();
        this.menuItemJson = new MenuItemJson();
        this.menuOptionJson = new MenuOptionJson();
        this.userJson = new UserJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details_view);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            final String orderId = extras.getString(SharedPreferenceUtils.KEY_ORDER_ID);

            ToolbarUtils.setToolbar(this, orderId);

            final LinearLayout orderOptionsView = findViewById(R.id.activity_order_details_view_value_order_options);
            final TextView orderStatusView = findViewById(R.id.activity_order_details_view_value_order_status);
            final TextView discountCodeView = findViewById(R.id.activity_order_details_view_value_discount_code);
            final TextView customerNameView = findViewById(R.id.activity_order_details_view_value_customer_name);
            final TextView customerContactNumberView = findViewById(R.id.activity_order_details_view_value_customer_contact_number);

            final Button assignToMeButton = findViewById(R.id.activity_order_details_view_button_assign_to_me);
            final Button cookingButton = findViewById(R.id.activity_order_details_view_button_cooking);
            final Button deliveryButton = findViewById(R.id.activity_order_details_view_button_delivery);

            orderRequestHandler.view(this, orderJson, orderId, order -> {
                orderStatusView.setText("Order Status: " + order.getOrderStatus());
                customerNameView.setText("Customer Name: " + order.getCustomerName());
                customerContactNumberView.setText("Contact Number: " + order.getCustomerContactNumber());
                discountCodeView.setText("Discount Code: " + StringUtils.stringOrBlank(order.getDiscountCode()));

                if (order.getOrderStatus().equalsIgnoreCase("COOKING"))
                    cookingButton.setVisibility(View.GONE);
                else if (order.getOrderStatus().equalsIgnoreCase("DELIVERY"))
                    deliveryButton.setVisibility(View.GONE);

                Spinner deliveryDriverDropdown = findViewById(R.id.component_dropdown);
                if (order.getOrderStatus().equalsIgnoreCase("DELIVERY") && StringUtils.stringOrBlank(order.getDeliveryDriver()).isEmpty()) {
                    userRequestHandler.listDeliveryDrivers(this, userJson, users -> {
                        deliveryDriverDropdown.setAdapter(new ArrayAdapter<>(
                                this, android.R.layout.simple_spinner_dropdown_item,
                                users.stream().map(user -> user.getForename() + " " + user.getSurname()).collect(Collectors.toList())
                        ));
                        deliveryDriverDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                deliveryDriver = users.get(position).getId();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });
                    });
                } else {
                    deliveryDriverDropdown.setVisibility(View.GONE);
                    assignToMeButton.setVisibility(View.GONE);
                    findViewById(R.id.activity_order_details_view_title_assign_delivery_driver).setVisibility(View.GONE);
                }

                order.getOrderOptions().forEach(orderOption -> {
                    if (!orderOption.getSelectedOptionIds().isEmpty())
                        menuItemRequestHandler.view(this, menuItemJson, orderOption.getMenuItemId(), menuItem ->
                                menuOptionRequestHandler.listByIds(
                                        this, menuOptionJson, orderOption.getSelectedOptionIds(), menuOptions -> {
                                            TextView menuItemView = new TextView(this);
                                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                            layoutParams.setMargins(0, 10, 0, 0);
                                            menuItemView.setLayoutParams(layoutParams);
                                            menuItemView.setTextColor(getResources().getColor(R.color.black));
                                            menuItemView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                                            menuItemView.setText(menuItem.getTitle());
                                            orderOptionsView.addView(menuItemView);
                                            menuOptions.forEach(mo -> {
                                                TextView menuOptionView = new TextView(this);
                                                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                                layoutParams2.setMargins(15, 0, 0, 0);
                                                menuOptionView.setLayoutParams(layoutParams2);
                                                menuOptionView.setTextColor(getResources().getColor(R.color.black));
                                                menuOptionView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                                                menuOptionView.setText("- " + mo.getTitle());
                                                orderOptionsView.addView(menuOptionView);
                                            });
                                        },
                                        error -> {
                                        }
                                )
                        );
                    else
                        menuItemRequestHandler.view(this, menuItemJson, orderOption.getMenuItemId(), menuItem -> {
                                    TextView menuItemView = new TextView(this);
                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    layoutParams.setMargins(0, 10, 0, 0);
                                    menuItemView.setLayoutParams(layoutParams);
                                    menuItemView.setTextColor(getResources().getColor(R.color.black));
                                    menuItemView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                                    menuItemView.setText(menuItem.getTitle());
                                    orderOptionsView.addView(menuItemView);
                                },
                                error -> {
                                }
                        );
                });
            });

            cookingButton.setOnClickListener(v ->
                    orderRequestHandler.updateOrderStatus(this, orderId, "COOKING"));

            deliveryButton.setOnClickListener(v ->
                    orderRequestHandler.updateOrderStatus(this, orderId, "DELIVERY"));

            assignToMeButton.setOnClickListener(v ->
                    orderRequestHandler.updateDeliveryDriver(this, orderJson, orderId, new Order(deliveryDriver)));
        }
    }
}
