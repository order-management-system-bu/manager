package uk.ac.bournemouth.i7626893.oms.manager.request.order;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order.Order;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestQueue;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.RequestBuilder;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for handling all Order
 * related HTTP requests.
 */
public class OrderRequestHandler extends RequestHandler<Order> {
    @Override
    protected String getViewEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/order/" + id + "/view";
    }

    @Override
    protected String getListEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/order/list";
    }

    @Override
    protected String getRemoveEndpoint(String id) {
        throw new RuntimeException("Removing orders is not permitted.");
    }

    @Override
    protected String getCreateEndpoint() {
        throw new RuntimeException("Creating orders is not permitted.");
    }

    @Override
    protected String getUpdateEndpoint(String id) {
        throw new RuntimeException("Updating orders is not permitted.");
    }

    private String getUpdateOrderStatus(String id, String orderStatus) {
        return ENDPOINT_RMW_BASE + "/om/order/" + id + "/" + orderStatus;
    }

    private String getListByOrderStatuses(List<String> orderStatuses) {
        return ENDPOINT_RMW_BASE + "/om/order/list/" + orderStatuses.stream().collect(Collectors.joining(","));
    }

    private String getUpdateDeliveryDriver(String deliveryDriverId) {
        return ENDPOINT_RMW_BASE + "/om/order/" + deliveryDriverId + "/delivery-driver";
    }

    private String getListByDeliveryDriver(String deliveryDriverId) {
        return ENDPOINT_RMW_BASE + "/om/order/" + deliveryDriverId + "/list/delivery-driver";
    }

    /**
     * Function is responsible for pushing a new update order status
     * request to the applications request queue.
     */
    public void updateOrderStatus(Context context, String id, String orderStatus) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.PUT(
                getUpdateOrderStatus(id, orderStatus), null, SecurityUtils.createAuthenticationHeader(context),
                success -> {
                    ((AppCompatActivity) context).finish();
                    Toast.makeText(context, SUCCESS, Toast.LENGTH_LONG).show();
                },
                error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        ));
    }

    /**
     * Function is responsible for pushing a new list orders by order
     * statuses request to the applications request queue.
     */
    public void listOrdersByOrderStatuses(Context context, JsonReader<Order> reader, List<String> orderStatuses, Consumer<List<Order>> onSuccess) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.LIST(
                getListByOrderStatuses(orderStatuses), SecurityUtils.createAuthenticationHeader(context), reader,
                onSuccess, error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        ));
    }

    /**
     * Function is responsible for pushing a new update delivery driver
     * request to the applications request queue.
     */
    public void updateDeliveryDriver(Context context, JsonWriter<Order> writer, String deliveryDriverId, Order order) {
        writer.write(order).ifPresent(json -> RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.PUT(
                getUpdateDeliveryDriver(deliveryDriverId), json, SecurityUtils.createAuthenticationHeader(context),
                success -> {
                    ((AppCompatActivity) context).finish();
                    Toast.makeText(context, SUCCESS, Toast.LENGTH_LONG).show();
                },
                error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        )));
    }

    /**
     * Function is responsible for pushing a new list by delivery driver request
     * to the applications database.
     */
    public void listOrdersByDeliveryDriver(Context context, JsonReader<Order> reader, String deliveryDriverId, Consumer<List<Order>> onSuccess) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.LIST(
                getListByDeliveryDriver(deliveryDriverId), SecurityUtils.createAuthenticationHeader(context), reader,
                onSuccess, error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Toast.makeText(context, UNAUTHORIZED, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, FAILURE, Toast.LENGTH_LONG).show();
                    }
                }
        ));
    }
}
