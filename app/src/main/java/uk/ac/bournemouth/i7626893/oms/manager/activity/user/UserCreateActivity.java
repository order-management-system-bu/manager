package uk.ac.bournemouth.i7626893.oms.manager.activity.user;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.Collections;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.user.UserJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user.User;
import uk.ac.bournemouth.i7626893.oms.manager.model.validation.ValidationGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.user.UserRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the user create Activity.
 */
public class UserCreateActivity extends AppCompatActivity {

    private final UserRequestHandler userRequestHandler;
    private final UserJson userJson;

    public UserCreateActivity() {
        this.userRequestHandler = new UserRequestHandler();
        this.userJson = new UserJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_create);

        ToolbarUtils.setToolbar(this, "Create an Account");

        final EditText usernameView = findViewById(R.id.form_user_signup_input_username);
        final CheckBox deliveryDriverView = findViewById(R.id.form_user_signup_input_delivery_driver);

        findViewById(R.id.form_user_signup_button_signup)
                .setOnClickListener(v -> userRequestHandler.createUser(
                        this, userJson,
                        new User(TextViewUtils.stringOrNull(usernameView), deliveryDriverView.isChecked()),
                        Collections.singletonList(new ValidationGroup(
                                "username", usernameView, findViewById(R.id.form_user_signup_validation_username)
                        )))
                );
    }
}
