package uk.ac.bournemouth.i7626893.oms.manager.activity.order.delivery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.order.OrderAdapter;
import uk.ac.bournemouth.i7626893.oms.manager.json.order.OrderJson;
import uk.ac.bournemouth.i7626893.oms.manager.request.order.OrderRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.RecyclerViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the view delivery driver specific orders Activity.
 */
public class OrderViewDeliveryActivity extends AppCompatActivity {

    private final OrderRequestHandler orderRequestHandler;
    private final OrderJson orderJson;
    private RecyclerView recyclerView;

    public OrderViewDeliveryActivity() {
        this.orderRequestHandler = new OrderRequestHandler();
        this.orderJson = new OrderJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_view_delivery);

        ToolbarUtils.setToolbar(this, "My Orders");

        this.recyclerView = findViewById(R.id.component_recycler);
        RecyclerViewUtils.renderBorders(this, recyclerView);
        updateRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateRecyclerView();
    }

    /**
     * Function is responsible for retrieving all Orders associated
     * with the current user and rendering the information in a RecyclerView.
     */
    private void updateRecyclerView() {
        SharedPreferenceUtils.getManagerId(this).ifPresent(id -> orderRequestHandler.listOrdersByDeliveryDriver(
                this, orderJson, id, orders -> recyclerView.setAdapter(new OrderAdapter(this, orders, ((position, v) -> {
                    if (orders.get(position).getOrderStatus().equalsIgnoreCase("DELIVERY")) {
                        Intent intent = new Intent(this, OrderDetailsViewDeliveryActivity.class);
                        intent.putExtra(SharedPreferenceUtils.KEY_ORDER_ID, orders.get(position).getId());
                        startActivity(intent);
                    } else {
                        Toast.makeText(this, "Details cannot be retrieved for this order", Toast.LENGTH_LONG).show();
                    }
                })))
        ));
    }
}
