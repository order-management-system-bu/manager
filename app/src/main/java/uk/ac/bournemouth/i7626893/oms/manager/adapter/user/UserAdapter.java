package uk.ac.bournemouth.i7626893.oms.manager.adapter.user;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.ClickListener;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user.User;

/**
 * Adapter class is responsible for rendering a RecyclerView for
 * instances of User.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private final List<User> users;
    private final LayoutInflater layoutInflater;
    private final ClickListener clickListener;

    public UserAdapter(Context context, List<User> users, ClickListener clickListener) {
        this.layoutInflater = LayoutInflater.from(context);
        this.users = users;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public UserAdapter.UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.component_recycler_row, parent, false);
        return new UserAdapter.UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.UserViewHolder holder, int position) {
        final String fullName = users.get(position).getForename() + " " + users.get(position).getSurname();
        holder.fullName.setText(fullName);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    /**
     * Class represents a ViewHolder class to be used
     * when rendering a recycler view.
     */
    class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView fullName;

        UserViewHolder(@NonNull View itemView) {
            super(itemView);
            fullName = itemView.findViewById(R.id.component_recycler_row_title);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
}
