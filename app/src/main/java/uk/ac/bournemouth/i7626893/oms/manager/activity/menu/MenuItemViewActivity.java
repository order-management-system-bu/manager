package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.activity.user.UserLoginActivity;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.GenericAdapter;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuCategoryJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuItemJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuCategory;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuItem;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuCategoryRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuItemRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.RecyclerViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the view MenuItem Activity.
 */
public class MenuItemViewActivity extends AppCompatActivity {

    private final MenuCategoryRequestHandler menuCategoryRequestHandler;
    private final MenuItemRequestHandler menuItemRequestHandler;
    private final MenuCategoryJson menuCategoryJson;
    private final MenuItemJson menuItemJson;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private RecyclerView recyclerView;
    private Spinner menuCategoryDropdown;
    private String selectedMenuCategory;

    public MenuItemViewActivity() {
        this.menuCategoryRequestHandler = new MenuCategoryRequestHandler();
        this.menuItemRequestHandler = new MenuItemRequestHandler();
        this.menuCategoryJson = new MenuCategoryJson();
        this.menuItemJson = new MenuItemJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_view);

        this.drawerLayout = findViewById(R.id.activity_menu_item_view_drawer_layout);
        this.navigationView = findViewById(R.id.activity_menu_item_view_navbar);
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.navbar_activity_menu_item_view_mc:
                    startActivity(new Intent(this, MenuCategoryViewActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_menu_item_view_mo:
                    startActivity(new Intent(this, MenuOptionViewActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_menu_item_view_mog:
                    startActivity(new Intent(this, MenuOptionGroupViewActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_menu_item_view_so:
                    SecurityUtils.storeAuthenticationToken(this, "");
                    Intent intent = new Intent(this, UserLoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;

                default:
                    drawerLayout.closeDrawers();
                    return true;
            }
        });

        ToolbarUtils.setToolbar(this, "Menu Items");
        ToolbarUtils.setSideMenuBar(this);
        ToolbarUtils.setupNavbarHeader(this, navigationView);

        this.recyclerView = findViewById(R.id.component_recycler);
        RecyclerViewUtils.renderBorders(this, recyclerView);
        this.menuCategoryDropdown = findViewById(R.id.component_dropdown);
        updateMenuCategoryDropdown();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateMenuCategoryDropdown();
        ToolbarUtils.setupNavbarHeader(this, navigationView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_view, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.appbar_generic_create:
                startActivity(new Intent(this, MenuItemCreateActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Function is responsible for retrieving all MenuItems
     * currently stored by the OMS ReactiveMiddleware application
     * and rendering the information as a RecyclerView.
     */
    private void updateRecyclerView(String menuCategoryId) {
        menuItemRequestHandler.listByMenuCategory(this, menuItemJson, menuCategoryId, menuItems ->
                recyclerView.setAdapter(new GenericAdapter<MenuItem>(this, menuItems, (position, v) -> {
                    Intent intent = new Intent(this, MenuItemUpdateActivity.class);
                    intent.putExtra(SharedPreferenceUtils.KEY_MENU_ITEM_ID, menuItems.get(position).getId());
                    startActivity(intent);
                }))
        );
    }

    /**
     * Function is responsible for retrieving all MenuCategories
     * currently stored by the OMS ReactiveMiddleware application
     * and rendering the information in a dropdown.
     */
    private void updateMenuCategoryDropdown() {
        menuCategoryRequestHandler.list(this, menuCategoryJson, menuCategories -> {
            menuCategoryDropdown.setAdapter(new ArrayAdapter<>(
                    this, android.R.layout.simple_spinner_dropdown_item,
                    menuCategories.stream().map(MenuCategory::getTitle).collect(Collectors.toList())
            ));
            menuCategoryDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedMenuCategory = menuCategories.get(position).getId();
                    updateRecyclerView(selectedMenuCategory);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            updateRecyclerView(menuCategories.get(0).getId());
        });
    }
}
