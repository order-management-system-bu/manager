package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuItemJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionGroupJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuItem;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOption;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOptionGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuItemRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionGroupRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;

/**
 * Class is responsible for providing the necessary functionality
 * for the menu option selection activity.
 */
public class MenuItemModificationSelectionActivity extends AppCompatActivity {

    private final MenuItemRequestHandler menuItemRequestHandler;
    private final MenuOptionRequestHandler menuOptionRequestHandler;
    private final MenuOptionGroupRequestHandler menuOptionGroupRequestHandler;
    private final MenuItemJson menuItemJson;
    private final MenuOptionJson menuOptionJson;
    private final MenuOptionGroupJson menuOptionGroupJson;

    public MenuItemModificationSelectionActivity() {
        this.menuItemRequestHandler = new MenuItemRequestHandler();
        this.menuOptionRequestHandler = new MenuOptionRequestHandler();
        this.menuOptionGroupRequestHandler = new MenuOptionGroupRequestHandler();
        this.menuItemJson = new MenuItemJson();
        this.menuOptionJson = new MenuOptionJson();
        this.menuOptionGroupJson = new MenuOptionGroupJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_modification_selection);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            final String menuItemId = extras.getString(SharedPreferenceUtils.KEY_MENU_ITEM_ID);

            ToolbarUtils.setToolbar(this, "Select Additional Options");

            final ListView menuOptionsView = findViewById(R.id.activity_menu_item_modification_selection_input_menu_options);
            final ListView menuOptionGroupsView = findViewById(R.id.activity_menu_item_modification_selection_input_menu_option_groups);

            menuItemRequestHandler.view(this, menuItemJson, menuItemId, menuItem -> {
                final List<MenuOption> menuOptions = new ArrayList<>();
                menuOptionRequestHandler.list(this, menuOptionJson, storedMenuOptions -> {
                    menuOptions.addAll(storedMenuOptions);
                    menuOptionsView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                    menuOptionsView.setAdapter(new ArrayAdapter<>(
                            this, android.R.layout.simple_list_item_single_choice,
                            storedMenuOptions.stream().map(MenuOption::getTitle).collect(Collectors.toList())
                    ));
                    menuItem.getAdditionalOptionIds().stream()
                            .flatMap(id -> storedMenuOptions.stream().filter(mo -> mo.getId().equals(id)))
                            .map(storedMenuOptions::indexOf).collect(Collectors.toList())
                            .forEach(position -> menuOptionsView.setItemChecked(position, true));
                });

                final List<MenuOptionGroup> menuOptionGroups = new ArrayList<>();
                menuOptionGroupRequestHandler.list(this, menuOptionGroupJson, storedMenuOptionGroups -> {
                    menuOptionGroups.addAll(storedMenuOptionGroups);
                    menuOptionGroupsView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                    menuOptionGroupsView.setAdapter(new ArrayAdapter<>(
                            this, android.R.layout.simple_list_item_single_choice,
                            storedMenuOptionGroups.stream().map(MenuOptionGroup::getTitle).collect(Collectors.toList())
                    ));
                    menuItem.getAdditionalOptionGroupIds().stream()
                            .flatMap(id -> storedMenuOptionGroups.stream().filter(mo -> mo.getId().equals(id)))
                            .map(storedMenuOptionGroups::indexOf).collect(Collectors.toList())
                            .forEach(position -> menuOptionsView.setItemChecked(position, true));
                });

                findViewById(R.id.component_button_submit).setOnClickListener(v -> {
                    final List<String> menuOptionIds = new ArrayList<>();
                    for (int i = 0; i < menuOptionsView.getCount(); i++)
                        if (menuOptionsView.getCheckedItemPositions().get(i))
                            menuOptionIds.add(menuOptions.get(i).getId());

                    final List<String> menuOptionGroupIds = new ArrayList<>();
                    for (int i = 0; i < menuOptionGroupsView.getCount(); i++)
                        if (menuOptionGroupsView.getCheckedItemPositions().get(i))
                            menuOptionGroupIds.add(menuOptionGroups.get(i).getId());

                    menuItemRequestHandler.updateSelections(this, menuItemJson, menuItemId, new MenuItem(
                            menuOptionIds, menuOptionGroupIds
                    ));
                });
            });
        }
    }
}
