package uk.ac.bournemouth.i7626893.oms.manager.utility.android;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.Objects;

import uk.ac.bournemouth.i7626893.oms.manager.R;

/**
 * Utility class is responsible for providing a range of
 * RecyclerView-related utility functions.
 */
public class RecyclerViewUtils {

    /**
     * Function is responsible for rendering the borders around
     * each row of the provided RecyclerView.
     */
    public static void renderBorders(Context context, RecyclerView recyclerView) {
        DividerItemDecoration itemDecoration = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(context, R.drawable.shape_border_recycler_row)));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(itemDecoration);
    }
}
