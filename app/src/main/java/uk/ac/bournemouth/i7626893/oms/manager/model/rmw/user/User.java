package uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;

/**
 * Model class represents a single User
 * associated with the OMS ReactiveMiddleware
 * application.
 */
@AllArgsConstructor
@Getter
public class User implements Model {
    private final String id;
    private final String userId;
    private final String username;
    private final String password;
    private final String forename;
    private final String surname;
    private final Boolean firstTimeLogin;
    private final Boolean admin;
    private final Boolean active;
    private final Boolean deliveryDriver;

    public User(String username, Boolean deliveryDriver) {
        this.username = username;
        this.deliveryDriver = deliveryDriver;

        this.id = null;
        this.userId = null;
        this.password = null;
        this.forename = null;
        this.surname = null;
        this.firstTimeLogin = null;
        this.admin = null;
        this.active = null;
    }

    public User(Boolean active, Boolean admin) {
        this.active = active;
        this.admin = admin;

        this.id = null;
        this.userId = null;
        this.username = null;
        this.password = null;
        this.forename = null;
        this.surname = null;
        this.firstTimeLogin = null;
        this.deliveryDriver = null;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;

        this.id = null;
        this.userId = null;
        this.forename = null;
        this.surname = null;
        this.firstTimeLogin = null;
        this.admin = null;
        this.active = null;
        this.deliveryDriver = null;
    }

    public User(String forename, String surname, String password) {
        this.forename = forename;
        this.surname = surname;
        this.password = password;

        this.id = null;
        this.userId = null;
        this.username = null;
        this.firstTimeLogin = null;
        this.admin = null;
        this.active = null;
        this.deliveryDriver = null;
    }
}