package uk.ac.bournemouth.i7626893.oms.manager.activity.user;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.Collections;
import java.util.List;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.user.UserJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user.User;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.validation.ValidationFault;
import uk.ac.bournemouth.i7626893.oms.manager.model.validation.ValidationGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.user.UserRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.StringUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the user update Activity.
 */
public class UserUpdateActivity extends AppCompatActivity {

    private final UserRequestHandler userRequestHandler;
    private final UserJson userJson;
    private String userId;

    public UserUpdateActivity() {
        this.userRequestHandler = new UserRequestHandler();
        this.userJson = new UserJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update);

        final EditText usernameView = findViewById(R.id.activity_user_update_value_username);
        final EditText forenameView = findViewById(R.id.form_user_input_forename);
        final EditText surnameView = findViewById(R.id.form_user_input_surname);
        final EditText passwordView = findViewById(R.id.form_user_input_password);
        final EditText confirmPasswordView = findViewById(R.id.form_user_input_confirm_password);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.userId = extras.getString(SharedPreferenceUtils.KEY_MANAGER_ID);
            userRequestHandler.view(this, userJson, userId, user -> {
                usernameView.setText(StringUtils.stringOrBlank(user.getUsername()));
                forenameView.setText(StringUtils.stringOrBlank(user.getForename()));
                surnameView.setText(StringUtils.stringOrBlank(user.getSurname()));
                ToolbarUtils.setToolbar(this, user.getForename() + " " + user.getSurname());
            });
        }

        final List<ValidationGroup> validationGroups = ValidationGroupUtils.createUserVg(
                this, forenameView, surnameView, passwordView, confirmPasswordView);

        findViewById(R.id.component_button_submit)
                .setOnClickListener(v -> {
                    if (!passwordView.getText().toString().isEmpty() && !passwordView.getText().toString().equals(confirmPasswordView.getText().toString())) {
                        TextViewUtils.displayValidationFaults(this, validationGroups, Collections.singletonList(
                                new ValidationFault("confirmPassword", "Passwords do not match.")
                        ));
                    } else {
                        userRequestHandler.update(this, userJson, userId, new User(
                                TextViewUtils.stringOrNull(forenameView),
                                TextViewUtils.stringOrNull(surnameView),
                                TextViewUtils.stringOrNull(passwordView)
                        ), validationGroups);
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_generic_remove:
                userRequestHandler.remove(this, userId);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
