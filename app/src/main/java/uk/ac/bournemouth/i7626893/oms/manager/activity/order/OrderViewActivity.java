package uk.ac.bournemouth.i7626893.oms.manager.activity.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.Arrays;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.activity.user.UserLoginActivity;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.order.OrderAdapter;
import uk.ac.bournemouth.i7626893.oms.manager.json.order.OrderJson;
import uk.ac.bournemouth.i7626893.oms.manager.request.order.OrderRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.RecyclerViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the view Order Activity.
 */
public class OrderViewActivity extends AppCompatActivity {

    private final OrderRequestHandler orderRequestHandler;
    private final OrderJson orderJson;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private RecyclerView recyclerView;

    public OrderViewActivity() {
        this.orderRequestHandler = new OrderRequestHandler();
        this.orderJson = new OrderJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_view);

        this.drawerLayout = findViewById(R.id.activity_order_view_drawer_layout);
        this.navigationView = findViewById(R.id.activity_order_view_navbar);
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.navbar_activity_order_view_so:
                    SecurityUtils.storeAuthenticationToken(this, "");
                    Intent intent = new Intent(this, UserLoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;

                default:
                    drawerLayout.closeDrawers();
                    return true;
            }
        });

        ToolbarUtils.setToolbar(this, "Pending Orders");
        ToolbarUtils.setSideMenuBar(this);
        ToolbarUtils.setupNavbarHeader(this, navigationView);

        this.recyclerView = findViewById(R.id.component_recycler);
        RecyclerViewUtils.renderBorders(this, recyclerView);
        updateRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateRecyclerView();
        ToolbarUtils.setupNavbarHeader(this, navigationView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Function is responsible for retrieving all Orders
     * currently stored by the OMS ReactiveMiddleware application
     * and rendering the information in a RecyclerView.
     */
    private void updateRecyclerView() {
        orderRequestHandler.listOrdersByOrderStatuses(this, orderJson, Arrays.asList("ACCEPTED", "COOKING", "DELIVERY"), orders ->
                recyclerView.setAdapter(new OrderAdapter(this, orders, ((position, v) -> {
                    Intent intent = new Intent(this, OrderDetailsViewActivity.class);
                    intent.putExtra(SharedPreferenceUtils.KEY_ORDER_ID, orders.get(position).getId());
                    startActivity(intent);
                })))
        );
    }
}
