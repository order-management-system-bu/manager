package uk.ac.bournemouth.i7626893.oms.manager.json.user;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user.User;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;

/**
 * Class is responsible for providing the JSON conversion
 * logic for all instances of User.
 */
public class UserJson implements JsonReader<User>, JsonWriter<User> {
    @Override
    public Optional<User> read(JSONObject object) {
        return Optional.of(new User(
                JsonUtils.stringOrNull(object, "id"),
                JsonUtils.stringOrNull(object, "userId"),
                JsonUtils.stringOrNull(object, "username"),
                null,
                JsonUtils.stringOrNull(object, "forename"),
                JsonUtils.stringOrNull(object, "surname"),
                JsonUtils.booleanOrNull(object, "firstTimeLogin"),
                JsonUtils.booleanOrNull(object, "admin"),
                JsonUtils.booleanOrNull(object, "active"),
                JsonUtils.booleanOrNull(object, "deliveryDriver")
        ));
    }

    @Override
    public Optional<JSONObject> write(User model) {
        try {
            JSONObject json = new JSONObject();
            JsonUtils.putIfNotNull(json, "username", model.getUsername());
            JsonUtils.putIfNotNull(json, "password", model.getPassword());
            JsonUtils.putIfNotNull(json, "forename", model.getForename());
            JsonUtils.putIfNotNull(json, "surname", model.getSurname());
            JsonUtils.putIfNotNull(json, "admin", model.getAdmin());
            JsonUtils.putIfNotNull(json, "active", model.getActive());
            JsonUtils.putIfNotNull(json, "deliveryDriver", model.getDeliveryDriver());
            return Optional.of(json);
        } catch (JSONException e) {
            return Optional.empty();
        }
    }
}
