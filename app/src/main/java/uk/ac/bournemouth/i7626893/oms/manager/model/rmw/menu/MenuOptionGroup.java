package uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu;

import java.util.List;

import lombok.Data;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;
import uk.ac.bournemouth.i7626893.oms.manager.model.ModelWithTitle;

/**
 * Model class represents a single MenuOptionGroupJson
 * associated with the OMS ReactiveMiddleware
 * application.
 */
@Data
public class MenuOptionGroup implements ModelWithTitle {
    private final String id;
    private final String title;
    private final List<String> menuItemOptions;
}
