package uk.ac.bournemouth.i7626893.oms.manager.request.menu;

import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOptionGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestHandler;

/**
 * Class is responsible for handling all MenuOptionGroup
 * related HTTP requests.
 */
public class MenuOptionGroupRequestHandler extends RequestHandler<MenuOptionGroup> {
    @Override
    protected String getViewEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option-group/" + id + "/view";
    }

    @Override
    protected String getListEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option-group/list";

    }

    @Override
    protected String getRemoveEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option-group/" + id + "/delete";
    }

    @Override
    protected String getCreateEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option-group";
    }

    @Override
    protected String getUpdateEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option-group/" + id + "/update";
    }
}
