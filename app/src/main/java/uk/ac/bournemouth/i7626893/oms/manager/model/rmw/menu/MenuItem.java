package uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu;

import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;
import uk.ac.bournemouth.i7626893.oms.manager.model.ModelWithTitle;

/**
 * Model class represents a single MenuItem
 * associated with the OMS ReactiveMiddleware
 * application.
 */
@AllArgsConstructor
@Getter
public class MenuItem implements ModelWithTitle {
    private final String id;
    private final String menuCategoryId;
    private final String title;
    private final String description;
    private final Double price;
    private final List<String> additionalOptionIds;
    private final List<String> additionalOptionGroupIds;

    public MenuItem(List<String> additionalOptionIds, List<String> additionalOptionGroupIds) {
        this.additionalOptionIds = additionalOptionIds;
        this.additionalOptionGroupIds = additionalOptionGroupIds;

        this.id = null;
        this.menuCategoryId = null;
        this.title = null;
        this.description = null;
        this.price = null;
    }

    public MenuItem(String menuCategoryId, String title, String description, Double price) {
        this.id = null;
        this.menuCategoryId = menuCategoryId;
        this.title = title;
        this.description = description;
        this.price = price;
        this.additionalOptionIds = Collections.emptyList();
        this.additionalOptionGroupIds = Collections.emptyList();
    }
}
