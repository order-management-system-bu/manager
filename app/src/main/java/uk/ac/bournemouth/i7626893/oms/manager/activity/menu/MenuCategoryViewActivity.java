package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.GenericAdapter;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuCategoryJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuCategory;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuCategoryRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.RecyclerViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the view MenuCategory Activity.
 */
public class MenuCategoryViewActivity extends AppCompatActivity {

    private final MenuCategoryRequestHandler menuCategoryRequestHandler;
    private final MenuCategoryJson menuCategoryJson;
    private RecyclerView recyclerView;

    public MenuCategoryViewActivity() {
        this.menuCategoryRequestHandler = new MenuCategoryRequestHandler();
        this.menuCategoryJson = new MenuCategoryJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_category_view);

        ToolbarUtils.setToolbar(this, "Menu Categories");

        this.recyclerView = findViewById(R.id.component_recycler);
        RecyclerViewUtils.renderBorders(this, recyclerView);
        updateRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_view, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_generic_create:
                startActivity(new Intent(this, MenuCategoryCreateActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Function is responsible for retrieving all MenuCategories
     * currently stored by the OMS ReactiveMiddleware application
     * and rendering the information in a RecyclerView.
     */
    private void updateRecyclerView() {
        menuCategoryRequestHandler.list(this, menuCategoryJson, menuCategories ->
                recyclerView.setAdapter(new GenericAdapter<MenuCategory>(this, menuCategories, (position, v) -> {
                    Intent intent = new Intent(this, MenuCategoryUpdateActivity.class);
                    intent.putExtra(SharedPreferenceUtils.KEY_MENU_CATEGORY_ID, menuCategories.get(position).getId());
                    startActivity(intent);
                }))
        );
    }
}
