package uk.ac.bournemouth.i7626893.oms.manager.utility.android;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.activity.user.UserUpdateActivity;

/**
 * Utility class is responsible for providing a range of
 * Toolbar-related utility functions.
 */
public class ToolbarUtils {

    /**
     * Function is responsible for setting an activities
     * Toolbar widget.
     */
    public static void setToolbar(Context context, String title) {
        AppCompatActivity activity = (AppCompatActivity) context;
        Toolbar toolbar = activity.findViewById(R.id.component_appbar);
        toolbar.setTitle(title);
        activity.setSupportActionBar(toolbar);
    }

    /**
     * Function is responsible for rendering the menu
     * button on the Toolbar widget.
     */
    public static void setSideMenuBar(Context context) {
        AppCompatActivity activity = (AppCompatActivity) context;
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        }
    }

    /**
     * Function is responsible for setting up a navbars header.
     */
    public static void setupNavbarHeader(Context context, NavigationView navigationView) {
        SharedPreferenceUtils.getManagerFullName(context).ifPresent(fullName -> {
            View headerView = navigationView.getHeaderView(0);
            if (headerView == null)
                headerView = navigationView.inflateHeaderView(R.layout.component_navbar_header);
            ((TextView) headerView.findViewById(R.id.component_navbar_header_full_name)).setText(fullName);
            headerView.findViewById(R.id.component_navbar_header_update).setOnClickListener(v -> {
                Intent intent = new Intent(context, UserUpdateActivity.class);
                SharedPreferenceUtils.getManagerId(context).ifPresent(managerId ->
                        intent.putExtra(SharedPreferenceUtils.KEY_MANAGER_ID, managerId));
                context.startActivity(intent);
            });
        });
    }
}
