package uk.ac.bournemouth.i7626893.oms.manager.utility.android;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.R;

/**
 * Utility class is responsible for providing a range of
 * Intent-related utility functions.
 */
public class SharedPreferenceUtils {

    public static final String KEY_MANAGER_ID = "manager.id";
    public static final String KEY_FULL_NAME = "manager.full_name";

    public static final String KET_USER_ID = "user.id";
    public static final String KEY_DISCOUNT_CODE_ID = "discountCode.id";
    public static final String KEY_MENU_CATEGORY_ID = "menuCategory.id";
    public static final String KEY_MENU_ITEM_ID = "menuItem.id";
    public static final String KEY_MENU_OPTION_ID = "menuOption.id";
    public static final String KEY_MENU_OPTION_GROUP_ID = "menuOptionGroup.id";
    public static final String KEY_ORDER_ID = "order.id";

    /**
     * Function is responsible for storing manager information to the
     * applications shared preferences.
     */
    public static void storeManagerPreferences(Context context, String managerId, String managerFullName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_MANAGER_ID, managerId).commit();
        sharedPreferences.edit().putString(KEY_FULL_NAME, managerFullName).commit();
    }

    /**
     * Function is responsible for retrieving the manager
     * ID from the activities context.
     */
    public static Optional<String> getManagerId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        final String managerId = sharedPreferences.getString(KEY_MANAGER_ID, null);
        return Optional.ofNullable(managerId);
    }

    /**
     * Function is responsible for retrieving the managers
     * full name from the activities context.
     */
    public static Optional<String> getManagerFullName(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        final String fullName = sharedPreferences.getString(KEY_FULL_NAME, null);
        return Optional.ofNullable(fullName);
    }
}
