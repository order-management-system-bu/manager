package uk.ac.bournemouth.i7626893.oms.manager.request.menu;

import android.content.Context;

import com.android.volley.VolleyError;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOption;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestQueue;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.RequestBuilder;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for handling all MenuOption
 * related HTTP requests.
 */
public class MenuOptionRequestHandler extends RequestHandler<MenuOption> {
    @Override
    protected String getViewEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option/" + id + "/view";
    }

    @Override
    protected String getListEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option/list";
    }

    @Override
    protected String getRemoveEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option/" + id + "/delete";
    }

    @Override
    protected String getCreateEndpoint() {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option";
    }

    @Override
    protected String getUpdateEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option/" + id + "/update";
    }

    private String getListByIdsEndpoint(List<String> ids) {
        return ENDPOINT_RMW_BASE + "/om/menu-item-option/" + ids.stream().collect(Collectors.joining(",")) + "/list";
    }

    /**
     * Function is responsible for pushing a new list menu options
     * by ids request to the applications request queue.
     */
    public void listByIds(
            Context context,
            JsonReader<MenuOption> reader,
            List<String> ids,
            Consumer<List<MenuOption>> onSuccess,
            Consumer<VolleyError> onFailure
    ) {
        RequestQueue.getInstance(context).addToRequestQueue(RequestBuilder.LIST(
                getListByIdsEndpoint(ids), SecurityUtils.createAuthenticationHeader(context), reader,
                onSuccess, onFailure
        ));
    }
}
