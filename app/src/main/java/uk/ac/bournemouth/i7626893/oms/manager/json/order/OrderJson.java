package uk.ac.bournemouth.i7626893.oms.manager.json.order;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order.Order;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order.OrderOption;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;

/**
 * Class is responsible for providing the JSON conversion
 * logic for all instances of Order.
 */
public class OrderJson implements JsonReader<Order>, JsonWriter<Order> {
    @Override
    public Optional<Order> read(JSONObject object) {
        return Optional.of(new Order(
                JsonUtils.stringOrNull(object, "id"),
                JsonUtils.stringOrNull(object, "orderStatus"),
                JsonUtils.doubleOrNull(object, "price"),
                readOrderOption(object),
                JsonUtils.stringOrNull(object, "discountCode"),
                JsonUtils.stringOrNull(object, "customerName"),
                JsonUtils.stringOrNull(object, "customerContactNumber"),
                JsonUtils.stringOrNull(object, "street"),
                JsonUtils.stringOrNull(object, "city"),
                JsonUtils.stringOrNull(object, "county"),
                JsonUtils.stringOrNull(object, "postcode1"),
                JsonUtils.stringOrNull(object, "postcode2"),
                JsonUtils.stringOrNull(object, "deliveryDriver")
        ));
    }

    private List<OrderOption> readOrderOption(JSONObject object) {
        try {
            final JSONArray jsonArray = object.getJSONArray("orderOptions");
            final List<OrderOption> orderOptions = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                final JSONObject orderOption = jsonArray.getJSONObject(i);
                final List<String> selectedOptionsIds = new ArrayList<>();
                if (orderOption.has("selectedOptionIds")) {
                    final JSONArray optionIds = orderOption.getJSONArray("selectedOptionIds");
                    for (int j = 0; j < optionIds.length(); j++)
                        selectedOptionsIds.add(optionIds.getString(j));
                }
                orderOptions.add(new OrderOption(orderOption.getString("menuItemId"), selectedOptionsIds));
            }
            return orderOptions;
        } catch (JSONException e) {
            return Collections.emptyList();
        }
    }

    @Override
    public Optional<JSONObject> write(Order model) {
        try {
            JSONObject json = new JSONObject();
            JsonUtils.putIfNotNull(json, "deliveryDriver", model.getDeliveryDriver());
            return Optional.of(json);
        } catch (JSONException e) {
            return Optional.empty();
        }
    }
}
