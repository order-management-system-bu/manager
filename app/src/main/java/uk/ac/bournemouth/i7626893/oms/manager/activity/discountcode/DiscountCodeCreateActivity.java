package uk.ac.bournemouth.i7626893.oms.manager.activity.discountcode;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.Arrays;
import java.util.List;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.discountcode.DiscountCodeJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.discountcode.DiscountCode;
import uk.ac.bournemouth.i7626893.oms.manager.request.discountcode.DiscountCodeRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the create DiscountCode Activity.
 */
public class DiscountCodeCreateActivity extends AppCompatActivity {

    private final DiscountCodeRequestHandler discountCodeRequestHandler;
    private final DiscountCodeJson discountCodeJson;
    private String discountType = "PERCENTAGE";

    public DiscountCodeCreateActivity() {
        this.discountCodeRequestHandler = new DiscountCodeRequestHandler();
        this.discountCodeJson = new DiscountCodeJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount_code_create);

        ToolbarUtils.setToolbar(this, "Create a discount code");

        Spinner discountTypeDropdown = findViewById(R.id.form_discount_code_input_discount_type);
        List<String> discountTypes = Arrays.asList("Percentage", "Fixed");
        discountTypeDropdown.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, discountTypes));
        discountTypeDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                discountType = discountTypes.get(position).toUpperCase();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        final EditText titleView = findViewById(R.id.form_discount_code_input_title);
        final EditText discountView = findViewById(R.id.form_discount_code_input_discount);
        final EditText spendEligibilityView = findViewById(R.id.form_discount_code_input_spend_eligibility);
        final CheckBox enabled = findViewById(R.id.form_discount_code_input_enabled);

        findViewById(R.id.component_button_submit)
                .setOnClickListener(v -> discountCodeRequestHandler
                        .create(this, discountCodeJson, new DiscountCode(
                                TextViewUtils.stringOrNull(titleView),
                                TextViewUtils.doubleOrNull(discountView),
                                discountType,
                                TextViewUtils.doubleOrNull(spendEligibilityView),
                                enabled.isEnabled()
                        ), ValidationGroupUtils.createDiscountCodeVg(
                                this, titleView, discountView, spendEligibilityView
                        )));
    }
}
