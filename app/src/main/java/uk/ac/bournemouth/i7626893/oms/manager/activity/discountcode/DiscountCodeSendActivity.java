package uk.ac.bournemouth.i7626893.oms.manager.activity.discountcode;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.discountcode.DiscountCodeJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.discountcode.DiscountCode;
import uk.ac.bournemouth.i7626893.oms.manager.request.discountcode.DiscountCodeRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the send DiscountCode Activity.
 */
public class DiscountCodeSendActivity extends AppCompatActivity {

    private final DiscountCodeRequestHandler discountCodeRequestHandler;
    private final DiscountCodeJson discountCodeJson;
    private String selectedDiscountCode;

    public DiscountCodeSendActivity() {
        this.discountCodeRequestHandler = new DiscountCodeRequestHandler();
        this.discountCodeJson = new DiscountCodeJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount_code_send);

        ToolbarUtils.setToolbar(this, "Send Discount Codes");

        Spinner discountCodeDropdown = findViewById(R.id.component_dropdown);
        discountCodeRequestHandler.list(this, discountCodeJson, discountCodes -> {
            discountCodeDropdown.setAdapter(new ArrayAdapter<>(
                    this, android.R.layout.simple_spinner_dropdown_item,
                    discountCodes.stream().map(DiscountCode::getTitle).collect(Collectors.toList())
            ));
            discountCodeDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedDiscountCode = discountCodes.get(position).getId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        });

        findViewById(R.id.component_button_submit)
                .setOnClickListener(v -> discountCodeRequestHandler.sendDiscountCode(this, selectedDiscountCode));
    }
}
