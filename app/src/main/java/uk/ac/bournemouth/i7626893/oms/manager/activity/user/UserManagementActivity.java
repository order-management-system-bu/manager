package uk.ac.bournemouth.i7626893.oms.manager.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.List;
import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.user.UserAdapter;
import uk.ac.bournemouth.i7626893.oms.manager.json.user.UserJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user.User;
import uk.ac.bournemouth.i7626893.oms.manager.request.user.UserRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.RecyclerViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.security.SecurityUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the user management Activity.
 */
public class UserManagementActivity extends AppCompatActivity {

    private final UserRequestHandler userRequestHandler;
    private final UserJson userJson;

    private RecyclerView recyclerView;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    public UserManagementActivity() {
        this.userRequestHandler = new UserRequestHandler();
        this.userJson = new UserJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_management);

        this.recyclerView = findViewById(R.id.component_recycler);
        updateRecyclerView();
        RecyclerViewUtils.renderBorders(this, recyclerView);

        this.drawerLayout = findViewById(R.id.activity_user_management_drawer_layout);
        this.navigationView = findViewById(R.id.activity_user_management_navbar);
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.navbar_activity_user_management_cu:
                    startActivity(new Intent(this, UserCreateActivity.class));
                    drawerLayout.closeDrawers();
                    return true;

                case R.id.navbar_activity_user_management_so:
                    SecurityUtils.storeAuthenticationToken(this, "");
                    Intent intent = new Intent(this, UserLoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;

                default:
                    drawerLayout.closeDrawers();
                    return true;
            }
        });

        ToolbarUtils.setToolbar(this, "User Management");
        ToolbarUtils.setSideMenuBar(this);
        ToolbarUtils.setupNavbarHeader(this, navigationView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateRecyclerView();
        ToolbarUtils.setupNavbarHeader(this, navigationView);
    }

    /**
     * Function is responsible for retrieving all Users
     * currently stored by the OMS ReactiveMiddleware
     * application and rendering the information in a
     * RecyclerView.
     */
    private void updateRecyclerView() {
        userRequestHandler.list(this, userJson, users -> {
                    List<User> completeUsers = users.stream().filter(u -> !u.getFirstTimeLogin()).collect(Collectors.toList());
                    recyclerView.setAdapter(new UserAdapter(this, completeUsers, ((position, v) -> {
                        Intent intent = new Intent(this, UserUpdateAdminActivity.class);
                        intent.putExtra(SharedPreferenceUtils.KEY_MANAGER_ID, completeUsers.get(position).getId());
                        intent.putExtra(SharedPreferenceUtils.KET_USER_ID, completeUsers.get(position).getUserId());
                        startActivity(intent);
                    })));
                }
        );
    }
}
