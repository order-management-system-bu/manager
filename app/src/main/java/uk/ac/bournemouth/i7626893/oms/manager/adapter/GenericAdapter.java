package uk.ac.bournemouth.i7626893.oms.manager.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.model.ModelWithTitle;

/**
 * Adapter class is responsible for rendering a RecyclerView for
 * instance of M.
 */
public class GenericAdapter<M extends ModelWithTitle> extends RecyclerView.Adapter<GenericAdapter.GenericViewHolder> {

    private final LayoutInflater layoutInflater;
    private final ClickListener clickListener;
    private List<M> models;

    public GenericAdapter(Context context, List<M> models, ClickListener clickListener) {
        this.layoutInflater = LayoutInflater.from(context);
        this.models = models;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.component_recycler_row, parent, false);
        return new GenericViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GenericAdapter.GenericViewHolder holder, int position) {
        holder.textView.setText(models.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    /**
     * Class represents a generic ViewHolder class to be used
     * when rendering a recycler view.
     */
    class GenericViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;

        GenericViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.component_recycler_row_title);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
}
