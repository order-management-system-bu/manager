package uk.ac.bournemouth.i7626893.oms.manager.activity.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import java.util.Collections;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.order.OrderAdapter;
import uk.ac.bournemouth.i7626893.oms.manager.json.order.OrderJson;
import uk.ac.bournemouth.i7626893.oms.manager.request.order.OrderRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.RecyclerViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the view previous orders Activity.
 */
public class PreviousOrderViewActivity extends AppCompatActivity {

    private final OrderRequestHandler orderRequestHandler;
    private final OrderJson orderJson;
    private RecyclerView recyclerView;

    public PreviousOrderViewActivity() {
        this.orderRequestHandler = new OrderRequestHandler();
        this.orderJson = new OrderJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_order_view);

        ToolbarUtils.setToolbar(this, "Completed Orders");

        this.recyclerView = findViewById(R.id.component_recycler);
        RecyclerViewUtils.renderBorders(this, recyclerView);
        updateRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateRecyclerView();
    }

    /**
     * Function is responsible for retrieving all Orders
     * currently stored by the OMS ReactiveMiddleware application
     * and rendering the information in a RecyclerView.
     */
    private void updateRecyclerView() {
        orderRequestHandler.listOrdersByOrderStatuses(this, orderJson, Collections.singletonList("COMPLETED"), orders ->
                recyclerView.setAdapter(new OrderAdapter(this, orders, ((position, v) -> {
                    Intent intent = new Intent(this, PreviousOrderDetailsViewActivity.class);
                    intent.putExtra(SharedPreferenceUtils.KEY_ORDER_ID, orders.get(position).getId());
                    startActivity(intent);
                })))
        );
    }
}
