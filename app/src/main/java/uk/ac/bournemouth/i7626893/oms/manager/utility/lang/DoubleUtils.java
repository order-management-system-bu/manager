package uk.ac.bournemouth.i7626893.oms.manager.utility.lang;

/**
 * Utility class is responsible for providing a range
 * or Double-related utility functions.
 */
public class DoubleUtils {
    /**
     * Function is responsible for evaluating a given Double
     * and returning an empty String if null.
     */
    public static String stringOrBlank(Double d) {
        return d == null ? "" : d.toString();
    }
}
