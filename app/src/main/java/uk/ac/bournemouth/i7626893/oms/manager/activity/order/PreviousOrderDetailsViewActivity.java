package uk.ac.bournemouth.i7626893.oms.manager.activity.order;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuItemJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.order.OrderJson;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuItemRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.order.OrderRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.DoubleUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.StringUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the view previous order details Activity.
 */
public class PreviousOrderDetailsViewActivity extends AppCompatActivity {

    private final OrderRequestHandler orderRequestHandler;
    private final MenuItemRequestHandler menuItemRequestHandler;
    private final MenuOptionRequestHandler menuOptionRequestHandler;
    private final OrderJson orderJson;
    private final MenuItemJson menuItemJson;
    private final MenuOptionJson menuOptionJson;

    public PreviousOrderDetailsViewActivity() {
        this.orderRequestHandler = new OrderRequestHandler();
        this.menuItemRequestHandler = new MenuItemRequestHandler();
        this.menuOptionRequestHandler = new MenuOptionRequestHandler();
        this.orderJson = new OrderJson();
        this.menuItemJson = new MenuItemJson();
        this.menuOptionJson = new MenuOptionJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_order_details_view);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            final String orderId = extras.getString(SharedPreferenceUtils.KEY_ORDER_ID);

            ToolbarUtils.setToolbar(this, orderId);

            final TextView orderStatusView = findViewById(R.id.activity_previous_order_details_view_value_order_status);
            final TextView discountCodeView = findViewById(R.id.activity_previous_order_details_view_value_discount_code);
            final TextView priceView = findViewById(R.id.activity_previous_order_details_view_value_price);
            final LinearLayout orderOptionsView = findViewById(R.id.activity_previous_order_details_view_value_order_options);

            orderRequestHandler.view(this, orderJson, orderId, order -> {
                orderStatusView.setText("Order Status: " + order.getOrderStatus());
                discountCodeView.setText("Discount Code: " + StringUtils.stringOrBlank(order.getDiscountCode()));
                priceView.setText("Price: " + DoubleUtils.stringOrBlank(order.getPrice()));

                order.getOrderOptions().forEach(orderOption -> {
                    if (!orderOption.getSelectedOptionIds().isEmpty())
                        menuItemRequestHandler.view(this, menuItemJson, orderOption.getMenuItemId(), menuItem ->
                                menuOptionRequestHandler.listByIds(
                                        this, menuOptionJson, orderOption.getSelectedOptionIds(), menuOptions -> {
                                            TextView menuItemView = new TextView(this);
                                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                            layoutParams.setMargins(0, 10, 0, 0);
                                            menuItemView.setLayoutParams(layoutParams);
                                            menuItemView.setTextColor(getResources().getColor(R.color.black));
                                            menuItemView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                                            menuItemView.setText(menuItem.getTitle());
                                            orderOptionsView.addView(menuItemView);
                                            menuOptions.forEach(mo -> {
                                                TextView menuOptionView = new TextView(this);
                                                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                                layoutParams2.setMargins(15, 0, 0, 0);
                                                menuOptionView.setLayoutParams(layoutParams2);
                                                menuOptionView.setTextColor(getResources().getColor(R.color.black));
                                                menuOptionView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                                                menuOptionView.setText("- " + mo.getTitle());
                                                orderOptionsView.addView(menuOptionView);
                                            });
                                        },
                                        error -> {
                                        }
                                )
                        );
                    else
                        menuItemRequestHandler.view(this, menuItemJson, orderOption.getMenuItemId(), menuItem -> {
                                    TextView menuItemView = new TextView(this);
                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    layoutParams.setMargins(0, 10, 0, 0);
                                    menuItemView.setLayoutParams(layoutParams);
                                    menuItemView.setTextColor(getResources().getColor(R.color.black));
                                    menuItemView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                                    menuItemView.setText(menuItem.getTitle());
                                    orderOptionsView.addView(menuItemView);
                                },
                                error -> {
                                }
                        );
                });
            });
        }
    }
}
