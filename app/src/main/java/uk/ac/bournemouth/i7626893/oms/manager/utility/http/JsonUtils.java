package uk.ac.bournemouth.i7626893.oms.manager.utility.http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Utility class is responsible for providing a range of
 * JSON-related utility functions.
 */
public class JsonUtils {
    /**
     * Function is responsible for attempting to retrieve
     * a Double from the provided JSONObject with the given
     * key.
     */
    public static Double doubleOrNull(JSONObject json, String key) {
        try {
            return json.getDouble(key);
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Function is responsible for attempting to retrieve
     * a String from the provided JSONObject with the given
     * key.
     */
    public static String stringOrNull(JSONObject json, String key) {
        try {
            return json.getString(key);
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Function is responsible for attempting to retrieve
     * a Boolean from the provided JSONObject with the given
     * key.
     */
    public static Boolean booleanOrNull(JSONObject json, String key) {
        try {
            return json.getBoolean(key);
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Function is responsible for attempting to retrieve
     * a List<String> from the provided JSONObject with the
     * given key.
     */
    public static List<String> stringListOrEmpty(JSONObject json, String key) {
        try {
            final JSONArray array = json.getJSONArray(key);
            final List<String> list = new ArrayList<>();
            for (int i = 0; i < array.length(); i++)
                list.add(array.getString(i));
            return list;
        } catch (JSONException e) {
            return Collections.emptyList();
        }
    }

    /**
     * Function is responsible for putting a new key/value
     * pair in the provided JSONObject if the value is
     * not null.
     */
    public static void putIfNotNull(JSONObject json, String key, Object value) throws JSONException {
        if (value != null)
            json.put(key, value);
    }

    /**
     * Function is responsible for putting a new key/value
     * pair in the provided JSONObject if the collection
     * is not empty.
     */
    public static <T> void putIfNotNull(JSONObject json, String key, Collection<T> value) throws JSONException {
        if (!value.isEmpty())
            json.put(key, new JSONArray(value));
    }
}
