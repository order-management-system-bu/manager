package uk.ac.bournemouth.i7626893.oms.manager.json.menu;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOptionGroup;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;

/**
 * Class is responsible for providing the JSON conversion
 * logic for all instances of MenuOptionGroup.
 */
public class MenuOptionGroupJson implements JsonReader<MenuOptionGroup>, JsonWriter<MenuOptionGroup> {
    @Override
    public Optional<MenuOptionGroup> read(JSONObject object) {
        return Optional.of(new MenuOptionGroup(
                JsonUtils.stringOrNull(object, "id"),
                JsonUtils.stringOrNull(object, "title"),
                JsonUtils.stringListOrEmpty(object, "menuItemOptions")
        ));
    }

    @Override
    public Optional<JSONObject> write(MenuOptionGroup model) {
        try {
            JSONObject json = new JSONObject();
            JsonUtils.putIfNotNull(json, "title", model.getTitle());
            JsonUtils.putIfNotNull(json, "menuItemOptions", model.getMenuItemOptions());
            return Optional.of(json);
        } catch (JSONException e) {
            return Optional.empty();
        }
    }
}
