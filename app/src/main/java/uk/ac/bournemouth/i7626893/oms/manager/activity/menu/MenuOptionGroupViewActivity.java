package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.GenericAdapter;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionGroupJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOptionGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionGroupRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.RecyclerViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the view MenuOptionGroup Activity.
 */
public class MenuOptionGroupViewActivity extends AppCompatActivity {

    private final MenuOptionGroupRequestHandler menuOptionGroupRequestHandler;
    private final MenuOptionGroupJson menuOptionGroupJson;
    private RecyclerView recyclerView;

    public MenuOptionGroupViewActivity() {
        this.menuOptionGroupRequestHandler = new MenuOptionGroupRequestHandler();
        this.menuOptionGroupJson = new MenuOptionGroupJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_option_group_view);

        ToolbarUtils.setToolbar(this, "Menu Option Groups");

        this.recyclerView = findViewById(R.id.component_recycler);
        RecyclerViewUtils.renderBorders(this, recyclerView);
        updateRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_view, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_generic_create:
                startActivity(new Intent(this, MenuOptionGroupCreateActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Function is responsible for retrieving all MenuOptionGroups
     * currently stored by the OMS ReactiveMiddleware application
     * and rendering the information a a RecyclerView.
     */
    private void updateRecyclerView() {
        menuOptionGroupRequestHandler.list(this, menuOptionGroupJson, menuOptionGroups ->
                recyclerView.setAdapter(new GenericAdapter<MenuOptionGroup>(this, menuOptionGroups, ((position, v) -> {
                    Intent intent = new Intent(this, MenuOptionGroupUpdateActivity.class);
                    intent.putExtra(SharedPreferenceUtils.KEY_MENU_OPTION_GROUP_ID, menuOptionGroups.get(position).getId());
                    startActivity(intent);
                })))
        );
    }
}
