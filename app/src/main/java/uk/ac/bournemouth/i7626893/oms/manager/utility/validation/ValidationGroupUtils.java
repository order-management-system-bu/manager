package uk.ac.bournemouth.i7626893.oms.manager.utility.validation;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.model.validation.ValidationGroup;

/**
 * Utility class is responsible for providing a range
 * of validation group functions.
 */
public class ValidationGroupUtils {

    /**
     * Utility function is responsible for constructing a DiscountCode
     * validation group.
     */
    public static List<ValidationGroup> createDiscountCodeVg(
            Context context,
            EditText titleView,
            EditText discountView,
            EditText spendEligibilityView
    ) {
        AppCompatActivity activity = (AppCompatActivity) context;
        return Arrays.asList(
                new ValidationGroup("title", titleView, activity.findViewById(R.id.form_discount_code_validation_title)),
                new ValidationGroup("discount", discountView, activity.findViewById(R.id.form_discount_code_validation_discount)),
                new ValidationGroup("spendEligibility", spendEligibilityView, activity.findViewById(R.id.form_discount_code_validation_spend_eligibility))
        );
    }

    /**
     * Utility function is responsible for constructing a MenuCategory
     * validation group.
     */
    public static List<ValidationGroup> createMenuCategoryVg(Context context, EditText titleView, EditText descriptionView) {
        AppCompatActivity activity = (AppCompatActivity) context;
        return Arrays.asList(
                new ValidationGroup("title", titleView, activity.findViewById(R.id.form_menu_category_validation_title)),
                new ValidationGroup("description", descriptionView, activity.findViewById(R.id.form_menu_category_validation_description))
        );
    }

    /**
     * Utility function is responsible for constructing a MenuItem
     * validation group.
     */
    public static List<ValidationGroup> createMenuItemVg(Context context, EditText titleView, EditText descriptionView, EditText priceView) {
        AppCompatActivity activity = (AppCompatActivity) context;
        return Arrays.asList(
                new ValidationGroup("title", titleView, activity.findViewById(R.id.form_menu_item_validation_title)),
                new ValidationGroup("description", descriptionView, activity.findViewById(R.id.form_menu_item_validation_description)),
                new ValidationGroup("price", priceView, activity.findViewById(R.id.form_menu_item_validation_price))
        );
    }

    /**
     * Utility function is responsible for constructing MenuOption
     * validation group.
     */
    public static List<ValidationGroup> createMenuOptionVg(Context context, EditText titleView, EditText priceView) {
        AppCompatActivity activity = (AppCompatActivity) context;
        return Arrays.asList(
                new ValidationGroup("title", titleView, activity.findViewById(R.id.form_menu_item_option_validation_title)),
                new ValidationGroup("price", priceView, activity.findViewById(R.id.form_menu_item_option_validation_price))
        );
    }

    /**
     * Utility function is responsible for constructing MenuOptionGroup
     * validation group.
     */
    public static List<ValidationGroup> createMenuOptionGroupVg(Context context, EditText titleView) {
        AppCompatActivity activity = (AppCompatActivity) context;
        return Collections.singletonList(
                new ValidationGroup("title", titleView, activity.findViewById(R.id.form_menu_item_option_group_validation_title))
        );
    }

    /**
     * Utility function is responsible for constructing User
     * validation group.
     */
    public static List<ValidationGroup> createUserVg(
            Context context,
            EditText forenameView,
            EditText surnameView,
            EditText passwordView,
            EditText confirmPasswordView
    ) {
        AppCompatActivity activity = (AppCompatActivity) context;
        return Arrays.asList(
                new ValidationGroup("forename", forenameView, activity.findViewById(R.id.form_user_validation_forename)),
                new ValidationGroup("surname", surnameView, activity.findViewById(R.id.form_user_validation_surname)),
                new ValidationGroup("password", passwordView, activity.findViewById(R.id.form_user_validation_password)),
                new ValidationGroup("confirmPassword", confirmPasswordView, activity.findViewById(R.id.form_user_validation_confirm_password))
        );
    }

    /**
     * Utility function is responsible for constructing DeliveryOption
     * validation group.
     */
    public static List<ValidationGroup> createDeliveryOptionVg(Context context, EditText deliveryChargeView, EditText minimumDeliveryView) {
        AppCompatActivity activity = (AppCompatActivity) context;
        return Arrays.asList(
                new ValidationGroup("deliveryCharge", deliveryChargeView, activity.findViewById(R.id.form_delivery_options_validation_delivery_charge)),
                new ValidationGroup("minimumForDelivery", minimumDeliveryView, activity.findViewById(R.id.form_delivery_options_validation_minimum_delivery))
        );
    }
}
