package uk.ac.bournemouth.i7626893.oms.manager.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.toolbox.Volley;

/**
 * Class is responsible for creating a singleton request queue
 * that will handle thread pools when sending HTTP requests
 * to external services.
 */
public final class RequestQueue {
    private static RequestQueue instance;
    private static Context ctx;

    private com.android.volley.RequestQueue requestQueue;

    private RequestQueue(Context context) {
        ctx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized RequestQueue getInstance(Context context) {
        if (instance == null)
            instance = new RequestQueue(context);
        return instance;
    }

    private com.android.volley.RequestQueue getRequestQueue() {
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }
}
