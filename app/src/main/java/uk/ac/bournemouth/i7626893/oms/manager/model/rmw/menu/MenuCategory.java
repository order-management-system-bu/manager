package uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu;

import lombok.Data;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;
import uk.ac.bournemouth.i7626893.oms.manager.model.ModelWithTitle;

/**
 * Model class represents a single MenuCategory
 * associated with the OMS ReactiveMiddleware
 * application.
 */
@Data
public class MenuCategory implements ModelWithTitle {
    private final String id;
    private final String title;
    private final String description;
}
