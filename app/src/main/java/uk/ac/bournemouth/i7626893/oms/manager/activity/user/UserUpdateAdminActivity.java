package uk.ac.bournemouth.i7626893.oms.manager.activity.user;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.TextView;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.user.UserJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.user.User;
import uk.ac.bournemouth.i7626893.oms.manager.request.user.UserRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.StringUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the admin user update Activity.
 */
public class UserUpdateAdminActivity extends AppCompatActivity {

    private final UserRequestHandler userRequestHandler;
    private final UserJson userJson;
    private String userId;

    public UserUpdateAdminActivity() {
        this.userRequestHandler = new UserRequestHandler();
        this.userJson = new UserJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update_admin);

        final TextView usernameView = findViewById(R.id.activity_user_update_admin_value_username);
        final TextView forenameView = findViewById(R.id.activity_user_update_admin_value_forename);
        final TextView surnameView = findViewById(R.id.activity_user_update_admin_value_surname);
        final CheckBox enabledView = findViewById(R.id.activity_user_update_admin_input_enabled);
        final CheckBox adminView = findViewById(R.id.activity_user_update_admin_input_admin);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            final String managerId = extras.getString(SharedPreferenceUtils.KEY_MANAGER_ID);
            userRequestHandler.view(this, userJson, managerId, user -> {
                this.userId = user.getUserId();
                usernameView.setText(StringUtils.stringOrBlank(user.getUsername()));
                forenameView.setText(StringUtils.stringOrBlank(user.getForename()));
                surnameView.setText(StringUtils.stringOrBlank(user.getSurname()));
                enabledView.setChecked(user.getActive());
                adminView.setChecked(user.getAdmin());
                ToolbarUtils.setToolbar(this, user.getForename() + " " + user.getSurname());
            });
        }

        findViewById(R.id.component_button_submit).setOnClickListener(v ->
                userRequestHandler.adminUpdate(this, userJson, userId, new User(
                        enabledView.isChecked(), adminView.isChecked()
                )));
    }
}
