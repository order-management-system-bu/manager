package uk.ac.bournemouth.i7626893.oms.manager.adapter;

import android.view.View;

/**
 * Interface is responsible for providing the necessary
 * click listener functions.
 */
public interface ClickListener {
    void onItemClick(int position, View v);
}
