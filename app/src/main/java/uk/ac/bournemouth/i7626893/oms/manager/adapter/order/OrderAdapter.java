package uk.ac.bournemouth.i7626893.oms.manager.adapter.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.adapter.ClickListener;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order.Order;

/**
 * Adapter class is responsible for rendering a RecyclerView for
 * instances of Order.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    private final List<Order> orders;
    private final LayoutInflater layoutInflater;
    private final ClickListener clickListener;

    public OrderAdapter(Context context, List<Order> orders, ClickListener clickListener) {
        this.layoutInflater = LayoutInflater.from(context);
        this.orders = orders;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.component_recycler_row_order, parent, false);
        return new OrderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.OrderViewHolder holder, int position) {
        final String price = "£" + orders.get(position).getPrice().toString();
        holder.orderId.setText(orders.get(position).getId());
        holder.orderStatus.setText(orders.get(position).getOrderStatus());
        holder.orderPrice.setText(price);
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    /**
     * Class represents a ViewHolder class to be used
     * when rendering a recycler view.
     */
    class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView orderId, orderStatus, orderPrice;

        OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            orderId = itemView.findViewById(R.id.component_recycler_row_order_id);
            orderStatus = itemView.findViewById(R.id.component_recycler_row_order_status);
            orderPrice = itemView.findViewById(R.id.component_recycler_row_order_price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
}
