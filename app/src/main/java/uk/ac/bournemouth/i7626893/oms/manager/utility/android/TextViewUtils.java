package uk.ac.bournemouth.i7626893.oms.manager.utility.android;

import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.validation.ValidationFault;
import uk.ac.bournemouth.i7626893.oms.manager.model.validation.ValidationGroup;

/**
 * Utility class is responsible for providing a range of
 * TextView-related utility functions.
 */
public class TextViewUtils {
    /**
     * Function is responsible for retrieving a String from a
     * TextView.
     */
    public static String stringOrNull(TextView textView) {
        return textView.getText().toString().isEmpty() ? null : textView.getText().toString();
    }

    /**
     * Function is responsible for retrieving a Double from
     * a TextView.
     */
    public static Double doubleOrNull(TextView textView) {
        try {
            return textView.getText().toString().isEmpty() ? null : Double.valueOf(textView.getText().toString());
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Function is responsible for rendering red borders on form fields
     * that are invalid with error messages that are provided.
     */
    public static void displayValidationFaults(Context context, List<ValidationGroup> validationGroups, List<ValidationFault> faults) {
        validationGroups.forEach(vg -> {
            EditText input = vg.getInputView();
            TextView validation = vg.getValidationView();
            faults.stream().filter(f -> f.getField().equalsIgnoreCase(vg.getTitle())).findFirst().map(f -> {
                if (input.getInputType() == InputType.TYPE_TEXT_FLAG_MULTI_LINE)
                    input.setBackground(context.getDrawable(R.drawable.shape_border_edittext_failure));
                else input.setBackgroundTintList(context.getColorStateList(R.color.red));
                validation.setText(f.getFault());
                validation.setTextColor(context.getColor(R.color.red));
                validation.setVisibility(View.VISIBLE);
                return f;
            }).orElseGet(() -> {
                if (input.getInputType() == InputType.TYPE_TEXT_FLAG_MULTI_LINE)
                    input.setBackground(context.getDrawable(R.drawable.shape_border_edittext_multiline));
                else input.setBackgroundTintList(context.getColorStateList(R.color.colorPrimary));
                validation.setText("");
                validation.setVisibility(View.INVISIBLE);
                return null;
            });
        });
    }
}
