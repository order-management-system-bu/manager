package uk.ac.bournemouth.i7626893.oms.manager.json.discountcode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

import uk.ac.bournemouth.i7626893.oms.manager.json.JsonReader;
import uk.ac.bournemouth.i7626893.oms.manager.json.JsonWriter;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.discountcode.DiscountCode;
import uk.ac.bournemouth.i7626893.oms.manager.utility.http.JsonUtils;

/**
 * Class is responsible for providing the JSON conversion
 * logic for all instances of DiscountCode.
 */
public class DiscountCodeJson implements JsonReader<DiscountCode>, JsonWriter<DiscountCode> {
    @Override
    public Optional<DiscountCode> read(JSONObject object) {
        return Optional.of(new DiscountCode(
                JsonUtils.stringOrNull(object, "id"),
                JsonUtils.stringOrNull(object, "title"),
                JsonUtils.doubleOrNull(object, "discount"),
                JsonUtils.stringOrNull(object, "discountType"),
                JsonUtils.doubleOrNull(object, "spendEligibility"),
                JsonUtils.booleanOrNull(object, "enabled"),
                JsonUtils.stringOrNull(object, "code")
        ));
    }

    @Override
    public Optional<JSONObject> write(DiscountCode model) {
        try {
            JSONObject json = new JSONObject();
            JsonUtils.putIfNotNull(json, "title", model.getTitle());
            JsonUtils.putIfNotNull(json, "discount", model.getDiscount());
            JsonUtils.putIfNotNull(json, "discountType", model.getDiscountType());
            JsonUtils.putIfNotNull(json, "enabled", model.getEnabled());
            return Optional.of(json);
        } catch (JSONException e) {
            return Optional.empty();
        }
    }
}
