package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionGroupJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOption;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOptionGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionGroupRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary functionality
 * for the create MenuOptionGroup Activity.
 */
public class MenuOptionGroupCreateActivity extends AppCompatActivity {

    private final MenuOptionRequestHandler menuOptionRequestHandler;
    private final MenuOptionGroupRequestHandler menuOptionGroupRequestHandler;
    private final MenuOptionJson menuOptionJson;
    private final MenuOptionGroupJson menuOptionGroupJson;

    public MenuOptionGroupCreateActivity() {
        this.menuOptionRequestHandler = new MenuOptionRequestHandler();
        this.menuOptionGroupRequestHandler = new MenuOptionGroupRequestHandler();
        this.menuOptionJson = new MenuOptionJson();
        this.menuOptionGroupJson = new MenuOptionGroupJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_option_group_create);

        ToolbarUtils.setToolbar(this, "Create a Menu Option Group");

        final EditText titleView = findViewById(R.id.form_menu_item_option_group_input_title);
        final ListView menuOptionsView = findViewById(R.id.form_menu_item_option_group_input_menu_options);

        menuOptionsView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        final List<MenuOption> storedMenuOptions = new ArrayList<>();
        menuOptionRequestHandler.list(this, menuOptionJson, menuOptions -> {
            storedMenuOptions.addAll(menuOptions);
            menuOptionsView.setAdapter(new ArrayAdapter<>(
                    this, android.R.layout.simple_list_item_single_choice,
                    storedMenuOptions.stream().map(MenuOption::getTitle).collect(Collectors.toList())
            ));
        });

        findViewById(R.id.component_button_submit)
                .setOnClickListener(v -> menuOptionGroupRequestHandler
                        .create(this, menuOptionGroupJson, createMenuOptionGroup(
                                menuOptionsView, titleView, storedMenuOptions
                        ), ValidationGroupUtils.createMenuOptionGroupVg(this, titleView)));
    }

    /**
     * Utility function creates a new MenuOptionGroup instance.
     */
    private MenuOptionGroup createMenuOptionGroup(ListView menuOptionsView, EditText titleView, List<MenuOption> storedMenuOptions) {
        List<String> selectedOptions = new ArrayList<>();
        for (int i = 0; i < menuOptionsView.getAdapter().getCount(); i++)
            selectedOptions.add((String) menuOptionsView.getAdapter().getItem(i));
        return new MenuOptionGroup(
                null,
                TextViewUtils.stringOrNull(titleView),
                storedMenuOptions.stream()
                        .filter(mo -> selectedOptions.contains(mo.getTitle()))
                        .map(MenuOption::getId).collect(Collectors.toList())
        );
    }
}
