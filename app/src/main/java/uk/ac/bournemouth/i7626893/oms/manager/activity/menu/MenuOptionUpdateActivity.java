package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOption;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.DoubleUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.StringUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the update MenuOption Activity.
 */
public class MenuOptionUpdateActivity extends AppCompatActivity {

    private final MenuOptionRequestHandler menuOptionRequestHandler;
    private final MenuOptionJson menuOptionJson;
    private String menuItemOptionId;

    public MenuOptionUpdateActivity() {
        this.menuOptionRequestHandler = new MenuOptionRequestHandler();
        this.menuOptionJson = new MenuOptionJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_option_update);

        final EditText titleView = findViewById(R.id.form_menu_item_option_input_title);
        final EditText priceView = findViewById(R.id.form_menu_item_option_input_price);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.menuItemOptionId = extras.getString(SharedPreferenceUtils.KEY_MENU_OPTION_ID);
            menuOptionRequestHandler.view(this, menuOptionJson, menuItemOptionId, menuOption -> {
                titleView.setText(StringUtils.stringOrBlank(menuOption.getTitle()));
                priceView.setText(DoubleUtils.stringOrBlank(menuOption.getPrice()));
                ToolbarUtils.setToolbar(this, "Update " + menuOption.getTitle());
            });
        }

        findViewById(R.id.component_button_submit).setOnClickListener(v -> menuOptionRequestHandler
                .update(this, menuOptionJson, menuItemOptionId, new MenuOption(
                        null,
                        TextViewUtils.stringOrNull(titleView),
                        TextViewUtils.doubleOrNull(priceView)
                ), ValidationGroupUtils.createMenuOptionVg(
                        this, titleView, priceView
                )));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_generic_remove:
                menuOptionRequestHandler.remove(this, menuItemOptionId);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
