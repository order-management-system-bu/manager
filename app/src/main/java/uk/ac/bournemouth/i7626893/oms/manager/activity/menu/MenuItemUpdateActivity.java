package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuCategoryJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuItemJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuCategory;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuItem;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuCategoryRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuItemRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.DoubleUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.StringUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the update MenuItem Activity.
 */
public class MenuItemUpdateActivity extends AppCompatActivity {

    private final MenuCategoryRequestHandler menuCategoryRequestHandler;
    private final MenuItemRequestHandler menuItemRequestHandler;
    private final MenuCategoryJson menuCategoryJson;
    private final MenuItemJson menuItemJson;
    private Spinner menuCategoryDropdown;
    private String selectedMenuCategory, menuItemId;

    public MenuItemUpdateActivity() {
        this.menuCategoryRequestHandler = new MenuCategoryRequestHandler();
        this.menuItemRequestHandler = new MenuItemRequestHandler();
        this.menuCategoryJson = new MenuCategoryJson();
        this.menuItemJson = new MenuItemJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_update);

        this.menuCategoryDropdown = findViewById(R.id.component_dropdown);
        menuCategoryRequestHandler.list(this, menuCategoryJson, menuCategories -> {
            menuCategoryDropdown.setAdapter(new ArrayAdapter<>(
                    this, android.R.layout.simple_spinner_dropdown_item,
                    menuCategories.stream().map(MenuCategory::getTitle).collect(Collectors.toList())
            ));
            menuCategoryDropdown.setSelection(
                    menuCategories.indexOf(menuCategories
                            .stream()
                            .filter(mc -> mc.getId().equals(selectedMenuCategory))
                            .findFirst()
                            .orElse(null)
                    ));
            menuCategoryDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedMenuCategory = menuCategories.get(position).getId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        });

        final EditText titleView = findViewById(R.id.form_menu_item_input_title);
        final EditText descriptionView = findViewById(R.id.form_menu_item_input_description);
        final EditText priceView = findViewById(R.id.form_menu_item_input_price);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.menuItemId = extras.getString(SharedPreferenceUtils.KEY_MENU_ITEM_ID);
            menuItemRequestHandler.view(this, menuItemJson, menuItemId, menuItem -> {
                this.selectedMenuCategory = menuItem.getMenuCategoryId();
                titleView.setText(StringUtils.stringOrBlank(menuItem.getTitle()));
                descriptionView.setText(StringUtils.stringOrBlank(menuItem.getDescription()));
                priceView.setText(DoubleUtils.stringOrBlank(menuItem.getPrice()));
                ToolbarUtils.setToolbar(this, "Update " + menuItem.getTitle());
            });
        }

        findViewById(R.id.component_button_submit).setOnClickListener(v -> menuItemRequestHandler
                .update(this, menuItemJson, menuItemId, new MenuItem(
                        selectedMenuCategory,
                        TextViewUtils.stringOrNull(titleView),
                        TextViewUtils.stringOrNull(descriptionView),
                        TextViewUtils.doubleOrNull(priceView)
                ), ValidationGroupUtils.createMenuItemVg(
                        this, titleView, descriptionView, priceView
                )));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_generic_remove:
                menuItemRequestHandler.remove(this, menuItemId);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
