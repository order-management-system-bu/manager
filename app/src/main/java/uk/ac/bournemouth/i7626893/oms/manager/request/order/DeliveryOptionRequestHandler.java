package uk.ac.bournemouth.i7626893.oms.manager.request.order;

import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order.DeliveryOption;
import uk.ac.bournemouth.i7626893.oms.manager.request.RequestHandler;

/**
 * Class is responsible for handling all delivery option
 * related HTTP requests.
 */
public class DeliveryOptionRequestHandler extends RequestHandler<DeliveryOption> {
    @Override
    protected String getViewEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/delivery-option/view";
    }

    @Override
    protected String getListEndpoint() {
        throw new RuntimeException("Listing delivery options is not permitted.");
    }

    @Override
    protected String getRemoveEndpoint(String id) {
        throw new RuntimeException("Removing delivery options is not permitted.");
    }

    @Override
    protected String getCreateEndpoint() {
        throw new RuntimeException("Creating delivery options is not permitted.");
    }

    @Override
    protected String getUpdateEndpoint(String id) {
        return ENDPOINT_RMW_BASE + "/om/delivery-option";
    }
}
