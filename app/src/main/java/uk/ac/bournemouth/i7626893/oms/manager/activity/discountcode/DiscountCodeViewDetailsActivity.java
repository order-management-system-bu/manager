package uk.ac.bournemouth.i7626893.oms.manager.activity.discountcode;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.Collections;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.discountcode.DiscountCodeJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.discountcode.DiscountCode;
import uk.ac.bournemouth.i7626893.oms.manager.request.discountcode.DiscountCodeRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.DoubleUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.StringUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the update DiscountCode Activity.
 */
public class DiscountCodeViewDetailsActivity extends AppCompatActivity {

    private final DiscountCodeRequestHandler discountCodeRequestHandler;
    private final DiscountCodeJson discountCodeJson;
    private String discountCodeId;

    public DiscountCodeViewDetailsActivity() {
        this.discountCodeRequestHandler = new DiscountCodeRequestHandler();
        this.discountCodeJson = new DiscountCodeJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount_code_view_details);

        final TextView titleView = findViewById(R.id.activity_discount_code_view_details_title);
        final TextView discountView = findViewById(R.id.activity_discount_code_view_details_discount);
        final TextView discountTypeView = findViewById(R.id.activity_discount_code_view_details_discount_type);
        final TextView spendEligibilityView = findViewById(R.id.activity_discount_code_view_details_spend_eligibility);
        final CheckBox enabledView = findViewById(R.id.activity_discount_code_view_details_input_enabled);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.discountCodeId = extras.getString(SharedPreferenceUtils.KEY_DISCOUNT_CODE_ID);
            discountCodeRequestHandler.view(this, discountCodeJson, discountCodeId, discountCode -> {
                titleView.setText(StringUtils.stringOrBlank(discountCode.getTitle()));
                discountView.setText(DoubleUtils.stringOrBlank(discountCode.getDiscount()));
                discountTypeView.setText(StringUtils.stringOrBlank(discountCode.getDiscountType()));
                spendEligibilityView.setText(DoubleUtils.stringOrBlank(discountCode.getSpendEligibility()));
                enabledView.setChecked(discountCode.getEnabled());
                ToolbarUtils.setToolbar(this, "Update " + titleView.getText().toString());
            });
        }

        findViewById(R.id.component_button_submit).setOnClickListener(v -> discountCodeRequestHandler
                .update(this, discountCodeJson, discountCodeId, new DiscountCode(enabledView.isChecked()), Collections.emptyList()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_generic_remove:
                discountCodeRequestHandler.remove(this, discountCodeId);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
