package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionGroupJson;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuOptionJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOption;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuOptionGroup;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionGroupRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuOptionRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.StringUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the update MenuItemOption Activity.
 */
public class MenuOptionGroupUpdateActivity extends AppCompatActivity {

    private final MenuOptionRequestHandler menuOptionRequestHandler;
    private final MenuOptionGroupRequestHandler menuOptionGroupRequestHandler;
    private final MenuOptionJson menuOptionJson;
    private final MenuOptionGroupJson menuOptionGroupJson;
    private String menuItemOptionGroupId;

    public MenuOptionGroupUpdateActivity() {
        this.menuOptionRequestHandler = new MenuOptionRequestHandler();
        this.menuOptionGroupRequestHandler = new MenuOptionGroupRequestHandler();
        this.menuOptionJson = new MenuOptionJson();
        this.menuOptionGroupJson = new MenuOptionGroupJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_option_group_update);

        final EditText titleView = findViewById(R.id.form_menu_item_option_group_input_title);
        final ListView menuOptionsView = findViewById(R.id.form_menu_item_option_group_input_menu_options);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.menuItemOptionGroupId = extras.getString(SharedPreferenceUtils.KEY_MENU_OPTION_GROUP_ID);
            menuOptionGroupRequestHandler.view(this, menuOptionGroupJson, menuItemOptionGroupId, menuOptionGroup -> {
                titleView.setText(StringUtils.stringOrBlank(menuOptionGroup.getTitle()));
                ToolbarUtils.setToolbar(this, "Update " + menuOptionGroup.getTitle());

                menuOptionsView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                final List<MenuOption> menuOptions = new ArrayList<>();
                menuOptionRequestHandler.list(this, menuOptionJson, storedMenuOptions -> {
                    menuOptions.addAll(storedMenuOptions);
                    menuOptionsView.setAdapter(new ArrayAdapter<>(
                            this, android.R.layout.simple_list_item_single_choice,
                            storedMenuOptions.stream().map(MenuOption::getTitle).collect(Collectors.toList())
                    ));
                    menuOptionGroup.getMenuItemOptions().stream()
                            .flatMap(id -> storedMenuOptions.stream().filter(mo -> mo.getId().equals(id)))
                            .map(storedMenuOptions::indexOf).collect(Collectors.toList())
                            .forEach(position -> menuOptionsView.setItemChecked(position, true));
                });

                findViewById(R.id.component_button_submit).setOnClickListener(v -> {
                    final List<String> menuOptionIds = new ArrayList<>();
                    for (int i = 0; i < menuOptionsView.getCount(); i++)
                        if (menuOptionsView.getCheckedItemPositions().get(i))
                            menuOptionIds.add(menuOptions.get(i).getId());
                    menuOptionGroupRequestHandler.update(this, menuOptionGroupJson, menuItemOptionGroupId, new MenuOptionGroup(
                            null, TextViewUtils.stringOrNull(titleView), menuOptionIds
                    ), ValidationGroupUtils.createMenuOptionGroupVg(this, titleView));
                });
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_generic_remove:
                menuOptionGroupRequestHandler.remove(this, menuItemOptionGroupId);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
