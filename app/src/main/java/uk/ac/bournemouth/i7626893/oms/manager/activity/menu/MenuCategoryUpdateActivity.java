package uk.ac.bournemouth.i7626893.oms.manager.activity.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import uk.ac.bournemouth.i7626893.oms.manager.R;
import uk.ac.bournemouth.i7626893.oms.manager.json.menu.MenuCategoryJson;
import uk.ac.bournemouth.i7626893.oms.manager.model.rmw.menu.MenuCategory;
import uk.ac.bournemouth.i7626893.oms.manager.request.menu.MenuCategoryRequestHandler;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.SharedPreferenceUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.TextViewUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.android.ToolbarUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.lang.StringUtils;
import uk.ac.bournemouth.i7626893.oms.manager.utility.validation.ValidationGroupUtils;

/**
 * Class is responsible for providing the necessary
 * functionality for the update MenuCategory Activity.
 */
public class MenuCategoryUpdateActivity extends AppCompatActivity {

    private final MenuCategoryRequestHandler menuCategoryRequestHandler;
    private final MenuCategoryJson menuCategoryJson;
    private String menuCategoryId;

    public MenuCategoryUpdateActivity() {
        this.menuCategoryRequestHandler = new MenuCategoryRequestHandler();
        this.menuCategoryJson = new MenuCategoryJson();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_category_update);

        final EditText titleView = findViewById(R.id.form_menu_category_input_title);
        final EditText descriptionView = findViewById(R.id.form_menu_category_input_description);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.menuCategoryId = extras.getString(SharedPreferenceUtils.KEY_MENU_CATEGORY_ID);
            menuCategoryRequestHandler.view(this, menuCategoryJson, menuCategoryId, menuCategory -> {
                titleView.setText(StringUtils.stringOrBlank(menuCategory.getTitle()));
                descriptionView.setText(StringUtils.stringOrBlank(menuCategory.getDescription()));
                ToolbarUtils.setToolbar(this, "Update " + menuCategory.getTitle());
            });
        }

        findViewById(R.id.component_button_submit)
                .setOnClickListener(v -> menuCategoryRequestHandler
                        .update(this, menuCategoryJson, menuCategoryId, new MenuCategory(
                                null,
                                TextViewUtils.stringOrNull(titleView),
                                TextViewUtils.stringOrNull(descriptionView)
                        ), ValidationGroupUtils.createMenuCategoryVg(
                                this, titleView, descriptionView
                        )));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.appbar_generic_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_generic_remove:
                menuCategoryRequestHandler.remove(this, menuCategoryId);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
