package uk.ac.bournemouth.i7626893.oms.manager.model.rmw.order;

import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import uk.ac.bournemouth.i7626893.oms.manager.model.Model;

/**
 * Model class represents a single Order
 * associated with the OMS ReactiveMiddleware
 * application.
 */
@AllArgsConstructor
@Getter
public class Order implements Model {
    private final String id;
    private final String orderStatus;
    private final Double price;
    private final List<OrderOption> orderOptions;
    private final String discountCode;
    private final String customerName;
    private final String customerContactNumber;
    private final String street;
    private final String city;
    private final String county;
    private final String postcode1;
    private final String postcode2;
    private final String deliveryDriver;

    public Order(String deliveryDriver) {
        this.deliveryDriver = deliveryDriver;

        this.id = null;
        this.orderStatus = null;
        this.price = null;
        this.orderOptions = Collections.emptyList();
        this.discountCode = null;
        this.customerName = null;
        this.customerContactNumber = null;
        this.street = null;
        this.county = null;
        this.city = null;
        this.postcode1 = null;
        this.postcode2 = null;
    }
}
